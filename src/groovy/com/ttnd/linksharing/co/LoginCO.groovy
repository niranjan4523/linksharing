package com.ttnd.linksharing.co

import com.ttnd.linksharing.util.CommonUtils
import grails.validation.Validateable

/**
 * Created by niranjan on 21/1/16.
 */
@Validateable
class LoginCO {
    long userId;
    String username;
    String password;

    void setPassword(String password) {
        if(password && password.length() < 4) {
            return
        }
        this.password = password?CommonUtils.generateMd5(password):"";
    }

    static constraints = {
        userId (nullable: true, blank:true)
        password (minSize: 4)
    }
}
