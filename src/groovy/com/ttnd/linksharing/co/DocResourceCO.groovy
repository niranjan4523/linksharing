package com.ttnd.linksharing.co

import com.ttnd.linksharing.User
import grails.validation.Validateable
import groovy.transform.ToString
import org.springframework.web.multipart.commons.CommonsMultipartFile

/**
 * Created by niranjan on 21/1/16.
 */
@ToString
@Validateable
class DocResourceCO {
    CommonsMultipartFile document;
    String docDesc;
    int docTopic;
    User user;
    String fileLocation;
    String topicName;

    static constraints = {
        user (nullable: true,blank:true)
        fileLocation (nullable: true,blank:true)
        topicName (nullable: true,blank:true)
    }

    void uploadDoc(long resourceId) {
        File file = new File(fileLocation,"/docs/doc_${resourceId}_${document.fileItem.name}")
        file.setBytes(document.bytes)
        fileLocation = file.absolutePath
    }
}
