package com.ttnd.linksharing.co

import com.ttnd.linksharing.constants.Constant
import com.ttnd.linksharing.util.CommonUtils
import grails.plugin.mail.MailService
import grails.validation.Validateable
import groovy.transform.ToString
import org.springframework.web.multipart.commons.CommonsMultipartFile

/**
 * Created by niranjan on 18/1/16.
 */
@ToString
@Validateable
class RegisterUserCO {
    String firstName;
    String lastName;
    String email;
    String username;
    String password1;
    String password2;
    String fileLocation;
    String authenticationKey;
    CommonsMultipartFile photo;

    static constraints = {
        email (nullable: false,email: true)
        username (nullable: false)
        password1 validator: { password1, userCO ->
            password1 == userCO.password2
        }
        fileLocation (nullable: true,blank: true)
        authenticationKey (nullable: true,blank: true)
    }

    void setPassword1(String password1) {
        println(password1)
        this.password1 = password1?CommonUtils.generateMd5(password1):"";
    }
    void setPassword2(String password2) {
        this.password2 = password2?CommonUtils.generateMd5(password2):"";
    }

    void uploadPhoto(long userId) {
        String[] type = photo.fileItem.name.split("\\.")
        File file = new File(fileLocation,"/images/profile/user-image-${userId}"+(type.length==2?".${type[1]}":""))
        file.setBytes(photo.bytes)
        fileLocation = file.absolutePath.replace(fileLocation,"")
    }

    void sendActivationMail(MailService mailService,long userId) {
        mailService.sendMail {
            to email
            subject "LinkSharing Account Activation"
            body "Thank you for registering on ${Constant.DOMAIL_URL},\n" +
                    "\n" +
                    "Please click the link below to activate your account.\n" +
                    "\n" +
                    "${Constant.DOMAIL_URL}/user/verifyAndActivateUser?userId=${userId}&authKey=${authenticationKey}\n" +
                    "\n" +
                    "Regards\n" +
                    "Link Sharing Administration"

        }
    }
}
