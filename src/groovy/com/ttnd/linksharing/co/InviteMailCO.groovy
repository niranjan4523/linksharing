package com.ttnd.linksharing.co

import com.ttnd.linksharing.User
import grails.validation.Validateable

/**
 * Created by niranjan on 23/1/16.
 */
@Validateable
class InviteMailCO {
    String inviteEmail;
    int inviteTopic;
    User user;
    String subject;
    String body;
    static constraints = {
        user (nullable: true,blank:true)
        subject (nullable: true,blank:true)
        body (nullable: true,blank:true)
    }
}
