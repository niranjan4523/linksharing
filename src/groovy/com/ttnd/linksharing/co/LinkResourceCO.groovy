package com.ttnd.linksharing.co

import com.ttnd.linksharing.User
import grails.validation.Validateable
import groovy.transform.ToString

/**
 * Created by niranjan on 21/1/16.
 */
@ToString
@Validateable
class LinkResourceCO {
    String link;
    String linkDesc;
    User user;
    int linkTopic;
    String topicName;

    static constraints = {
        user (nullable: true,blank:true)
        topicName (nullable: true,blank:true)
    }
}
