package com.ttnd.linksharing.co

import com.ttnd.linksharing.User
import grails.validation.Validateable
import groovy.transform.ToString

/**
 * Created by niranjan on 21/1/16.
 */
@Validateable
@ToString
class TopicCO {
    String topicName;
    User user;
    int visibility;

    static constraints = {
        user (nullable: true,blank: true)
        visibility (nullable:false,inList:[1,2])
    }

}
