package com.ttnd.linksharing.util

import com.ttnd.linksharing.User

/**
 * Created by niranjan on 8/2/16.
 */
class SendActivationMail extends Thread {
    def closure;
    User user;
    SendActivationMail(def closure,User user) {
        this.closure = closure;
        this.user = user;
    }

    @Override
    void run() {
        closure(user);
    }
}
