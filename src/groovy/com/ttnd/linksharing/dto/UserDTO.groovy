package com.ttnd.linksharing.dto

/**
 * Created by niranjan on 22/1/16.
 */
class UserDTO {
    int id;
    String email;
    String username;
    String firstName;
    String lastName;
    String profilePicPath;
    boolean admin;
    boolean active;
    int subscriptionCount;
    int topicCount;
    Date dateCreated;
    Date lastUpdated;
}
