package com.ttnd.linksharing.dto

import com.ttnd.linksharing.enums.Seriousness
import com.ttnd.linksharing.enums.Visibility
import com.ttnd.linksharing.User
import groovy.transform.ToString

/**
 * Created by niranjan on 23/1/16.
 */
@ToString
class TopicDTO {
    int id;
    String name;
    Visibility visibility;
    Seriousness loggedInUserSeriousness;
    User createdBy;
    int subscriberCount;
    int resourceCount;
    List<UserDTO> subscriberList;
    List<ResourceDTO> resourceList

}
