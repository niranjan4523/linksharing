package com.ttnd.linksharing.dto

import com.ttnd.linksharing.Topic
import com.ttnd.linksharing.User

/**
 * Created by niranjan on 23/1/16.
 */
class ResourceDTO {
    long id;
    String description;
    User createdBy;
    long topicId;
    String topicName;
    Topic topic;
    String linkAction;
    String linkText;
    boolean isRead;
    Date dateCreated;
    int rating;
}
