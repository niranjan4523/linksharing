package com.ttnd.linksharing.enums

/**
 * Created by niranjan on 21/1/16.
 */
enum Visibility {
    PRIVATE(1),PUBLIC(2);
    final int value;
    Visibility(int value) {
        this.value = value;
    }
    public static Visibility getVisibilityEnum(int val) {
        switch (val) {
            case 1:
                return PRIVATE
            case 2:
                return PUBLIC
        }
    }
}