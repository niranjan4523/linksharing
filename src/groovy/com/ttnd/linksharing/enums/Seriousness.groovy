package com.ttnd.linksharing.enums

/**
 * Created by niranjan on 21/1/16.
 */
enum Seriousness {
    CASUAL(1),SERIOUS(2),VERY_SERIOUS(3);
    final int value;
    Seriousness(int value) {
        this.value = value;
    }
    public static Seriousness getSeriousness(int val) {
        switch (val) {
            case 1:
                return CASUAL
            case 2:
                return SERIOUS
            case 3:
                return VERY_SERIOUS
        }
    }
}