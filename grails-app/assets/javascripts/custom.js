/**
 * Created by niranjan on 28/1/16.
 */
$(document).ready(function(){
    $('.icon-comment').tooltip();
    $('.icon-envelope').tooltip();
    $('.icon-resize-small').tooltip();
    $('.icon-folder-close').tooltip();

    setTimeout(function(){
        $("div.alert.alert-success").hide()
        $("div.alert.alert-danger").hide()
    }, 5000);

    $('#searchButton').click(function () {
        $('#search-form').submit();
    });

    $('#login-form').validate({
        rules:{
            username: {
                required: true,
                username:true
            },
            password: {required: true,minlength: 4}
        },
        messages: {
            username: {
                required: "Please enter username",
            },
            password: {
                required: "Please enter password",
                minlength: "Your password must be at least 4 characters long"
            }
        },
        submitHandler: function (form) {
            $(form).find('input[type=submit]').attr('disabled','disabled');
            form.submit();
        }
    });


    $('#register-form').validate({
        rules:{
            firstName: {
                required: true,
                alpha:true
            },
            lastName: {
                required: true,
                alpha:true
            },
            username: {
                required: true,
                username:true,
                remote: {
                    url: checkUsernameUrl,
                    type: "post",
                    data: {
                        username: function() {
                            return $("#register-form input[name=username]").val().trim();
                        }
                    }
                }
            },
            email: {
                required: true,
                emailCheck: true,
                remote: {
                    url: checkUserEmailUrl,
                    type: "post",
                    data: {
                        email: function() {
                            return $("#register-form input[name=email]").val().trim();
                        }
                    }
                }
            },
            password1: {
                required: true,
                minlength: 4
            },
            password2: {
                required: true,
                equalTo: "#password1"
            }
        },
        messages: {
            firstName: {
                required:"Please enter first name"
            },
            lastName: {
                required:"Please enter last name"
            },
            username: {
                required: "Please enter username",
                remote: "Username not available"
            },
            password1: {
                required: "Please provide a password",
                minlength: "Your password must be at least 4 characters long"
            },
            password2: {
                required: "Please confirm password",
                equalTo: "Password and Confirm password should be same"
            },
            email: {
                required :"Please enter email address",
                emailCheck : "Please enter a valid email address",
                remote : "Email already used"
            }
        },
        submitHandler: function (form) {
            $(form).find('input[type=submit]').attr('disabled','disabled');
            form.submit();
        }
    })

    /*$('#send-invite').click(function () {
        alert(1)
        $.ajax({
            url: makeTopicDropDownUrl,
            method: "POST",
            success: function(result){
                $("#invite-user-form select[name=inviteTopic]").html(result);
            }
        });
    });*/
    
    $('#create-topic-form').validate({
        rules:{
            topicName: {
                required: true,
                alphaNumeric:true,
                remote: {
                    url: validateTopicNameUrl,
                    type: "post",
                    data: {
                        topicName: function() {
                            return $("#create-topic-form input[name=topicName]").val().trim();
                        }
                    }
                }
            }
        },
        messages: {
            topicName:{
                required: "Please enter topic name.",
                remote: "Topic name already used."
            }
        }
    });
    $('#invite-user-form').validate({
        rules:{
            inviteEmail: {
                required: true,
                emailCheck: true,
                remote: {
                    url: checkEmailRegisteredUrl,
                    type: "post",
                    data: {
                        email: function() {
                            return $("#invite-user-form input[name=inviteEmail]").val().trim();
                        }
                    }
                }
            }
        },
        messages:{
            required: "Please enter email id.",
            emailCheck:"Please enter valid email id.",
            remote:"This email is not registered with us."
        }
    });
    $(".sendInviteSingle").click(function () {
        var topicId = $(this).parents(".topicDiv").attr("topicId");
        $("#invite-user-form").find("select#inviteTopic").val(topicId);
    });
    $('#create-doc-form').validate({
        rules:{
            document: {required: true},
            docDesc: {required: true}
        },
        messages: {
            document: "Please select the document to upload.",
            docDesc: "Please enter document description"
        }
    });
    $('#create-link-form').validate({
        rules:{
            link: {
                required: true,
                url: true
            },
            linkDesc: {required: true}
        },
        messages: {
            link: {
                required: "Please enter link to share.",
                url: "Please enter a valid url."
            },
            linkDesc: "Please enter document description."
        }
    });

    $('#edit-profile-form').validate({
        rules:{
            firstName: {
                required: true,
                alpha:true
            },
            lastName: {
                required: true,
                alpha:true
            },
            username: {
                required: true,
                username:true,
                remote: {
                    url: checkUsernameUrl,
                    type: "post",
                    data: {
                        username: function() {
                            return $("#edit-profile-form input[name=username]").val().trim();
                        }
                    }
                }
            }
        },
        messages: {
            firstName: {
                required:"Please select the document to upload."
            },
            lastName: {
                required:"Please enter document description"
            },
            username: {
                required: "Please enter username",
                remote: "Username not available"
            }
        }
    });

    $('#change-password-form').validate({
        rules:{
            password1: {
                required: true,
                minlength: 4
            },
            password2: {
                required: true,
                equalTo: "#password1"
            }
        },
        messages: {
            password1: {
                required: "Please provide a password",
                minlength: "Your password must be at least 4 characters long"
            },
            password2: {
                required: "Please confirm password",
                equalTo: "Password and Confirm password should be same"
            }
        }
    });

    $(document).on('click','a.subscribe',function () {
        var obj = $(this);
        var event = obj.attr("event");
        var topicId = obj.parents(".topicDiv").attr("topicId");
        $.ajax({
            url: subscribeTopicUrl,
            method: "POST",
            data: {topicId:topicId,event:event},
            dataType: 'json',
            success: function(result){
                if(result.success) {
                    if(event == "subscribe") {
                        obj.text("Unsubscribe");
                        obj.attr("event","unsubscribe")
                    } else if(event == "unsubscribe") {
                        obj.text("Subscribe");
                        obj.attr("event","subscribe")
                    }
                }
                showAfterAjaxMessage(obj,result);
            }
        });
    });

    $(document).on('click','a.read',function () {
        var obj = $(this);
        var event = obj.attr("event");
        var postId = obj.attr("postId");
        $.ajax({
            url: readTopicUrl,
            method: "POST",
            data: {postId:postId,event:event},
            dataType: 'text',
            success: function(result){
                if(result == "done") {
                    if(event == "read") {
                        obj.text("Mark as unread");
                        obj.attr("event","unread")
                    } else if(event == "unread") {
                        obj.text("Mark as read");
                        obj.attr("event","read")
                    }
                }
            }
        });
    });

    $("select.topicVisibility").change(function(){
        var obj = $(this);
        var topicId = obj.parents(".topicDiv").attr("topicId");
        $.ajax({
            url: changeTopicVisibilityUrl,
            method: "POST",
            data: {topicId:topicId,visibility:$(this).val()},
            dataType: 'json',
            success: function(result){
                showAfterAjaxMessage(obj,result);
            }
        });
    });
    $("select.topicSeriousness").change(function(){
        var obj = $(this);
        var topicId = obj.parents(".topicDiv").attr("topicId");
        $.ajax({
            url: changeTopicSeriousnessUrl,
            method: "POST",
            data: {topicId:topicId,seriousness:$(this).val()},
            dataType: 'json',
            success: function(result){
                showAfterAjaxMessage(obj,result);
            }
        });
    });

    $("a.editTopic").click(function() {
        $(this).parent(".row-fluid").siblings(".topicNameDisplay").hide();
        $(this).parent(".row-fluid").siblings(".topicNameEdit").show();
    });

    $("a.saveTopicName").click(function() {
        var obj = $(this);
        var topicId = obj.parents(".topicDiv").attr("topicId");
        var topicName = obj.prev().val();
        if(topicName.trim() == "") {
            alert("Please enter the topic name")
            return;
        } else if(obj.parent("div").siblings(".topicNameDisplay").children("a").text().trim() == topicName) {
            alert("Topic Name unchanged.")
            return;
        }
        $.ajax({
            url: editTopicNameUrl,
            method: "POST",
            data: {topicId:topicId,topicName:topicName},
            dataType: 'json',
            success: function(result){
                if(result.success) {
                    obj.parent("div").siblings(".topicNameDisplay").children("a").text(topicName);
                }
                showAfterAjaxMessage(obj,result);
                obj.parent("div").siblings(".topicNameDisplay").show();
                obj.parents(".topicNameEdit").hide();
            }
        });
    });

    $("a.cancelTopicName").click(function() {
        $(this).parent("div").siblings(".topicNameDisplay").show();
        $(this).parents(".topicNameEdit").hide();
    });

    $("a.deleteTopic").click(function() {
        var topicId = $(this).parents(".topicDiv").attr("topicId");
        if(!confirm("Are you sure to delete this topic?")) {
            return;
        }
        $.ajax({
            url: deleteTopicUrl,
            method: "POST",
            data: {topicId:topicId},
            dataType: 'json',
            success: function(result){
            }
        });
    });

    $("a.showPostAjax").click(function() {
        var topicId = $(this).parents(".topicDiv").attr("topicId");
        var mainDivObj = $(this).parents(".topicDiv");
        $.ajax({
            url: showPostByTopicAjaxUrl,
            method: "POST",
            data: {topicId:topicId,offset:0,max:10},
            dataType: 'text',
            success: function(result){
                $("#ajax-post-outer-div").html(result);
                mainDivObj.siblings("div").attr("class","bottom-border topicDiv")
                mainDivObj.attr("class","bottom-border topicDiv selectedTopic")
            }
        });
    });
    $(".rating > span").click(function () {
        var obj = $(this);
        var postId = obj.parents(".rating").attr("resourceId");
        var score = obj.attr("value");
        $.ajax({
            url: saveResourceRatingUrl,
            method: "POST",
            data: {resourceId:postId,score:score},
            dataType: 'json',
            success: function(result){
                if(result.success) {
                    obj.siblings().attr('class','');
                    obj.addClass('selected')
                }
            }
        });
    });

    $("a.deleteResource").click(function() {
        var postId = $(this).attr("resourceId");
        if(!confirm("Are you sure to delete this Post?")) {
            return;
        }
        $.ajax({
            url: deletePostUrl,
            method: "POST",
            data: {resourceId:postId},
            dataType: 'json',
            success: function(result){
            }
        });
    });
});

function showAfterAjaxMessage(obj,result) {
    if(result.success) {
        obj.parents(".topicDiv").find("div.alert-success").text(result.success);
        obj.parents(".topicDiv").find("div.alert-success").show();
        setTimeout(function(){
            obj.parents(".topicDiv").find("div.alert-success").hide()
        }, 3000);
    } else if(result.error) {
        obj.parents(".topicDiv").find("div.alert-danger").text(result.error);
        obj.parents(".topicDiv").find("div.alert-danger").show();
        setTimeout(function(){
            obj.parents(".topicDiv").find("div.alert-danger").hide()
        }, 3000);
    }
}
