<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 19/1/16
  Time: 5:29 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="custom">
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span7">
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <div class="span2 margin-vertical">
                    <g:link controller="user" action="showUser" params="[userId:user.id]">
                        <img src="${user.profilePicPath}" width="100%" onerror="this.src='/images/profile/user-default.png'">
                    </g:link>
                </div>
                <div class="span10 margin-vertical">
                    <g:link controller="user" action="showUser" params="[userId:user.id]">
                        ${user.firstName} ${user.lastName}
                    </g:link>
                    <g:link controller="topic" action="viewOneTopic" params="${[topicId:post.topicId]}" class="pull-right">${post.topicName}</g:link><br>
                    <label style="display: inline" class="gray-text">@${user.username}</label>
                    <label style="display: inline" class="gray-text pull-right"><g:formatDate date="${post.dateCreated}" format="h:m a dd MMM yyyy"/></label>
                    <div class="rating" style="text-align: right" resourceId="${post.id}">
                        <span value="5" ${post.rating==5?"class=selected":""}>☆</span>
                        <span value="4" ${post.rating==4?"class=selected":""}>☆</span>
                        <span value="3" ${post.rating==3?"class=selected":""}>☆</span>
                        <span value="2" ${post.rating==2?"class=selected":""}>☆</span>
                        <span value="1" ${post.rating==1?"class=selected":""}>☆</span>
                    </div>
                </div>
                <div class="span12 margin">
                    ${post.description}
                </div>
                <div class="span12">
                    <g:if test="${(post.createdBy.id == session.user.id) || session.user.admin}">
                        <g:link onclick="return confirm('Are you sure to delete this resource?')" controller="resource" action="resourceDelete" params="${[resourceId: post.id]}">
                            Delete
                        </g:link>
                        <a href="#edit-resource" data-toggle="modal" class="margin-horizontal">
                            Edit
                        </a>
                    </g:if>
                    <g:link controller="resource" action="${post.linkAction}" params="${[resourceId:post.id]}" target="_blank" class="margin-horizontal">
                        ${post.linkText}
                    </g:link>
                </div>
            </div>
        </div>
        <div class="span5">
            <div class="navbar-inner" id="inbox-outer-div" style="margin-bottom: 20px;">
                <legend>Trending Topics</legend>
                <g:each in="${trendingTopics}" var="trendingTopicDTO">
                    <g:include view="dashboard/template/_topic.gsp" model="${[topicDTO:trendingTopicDTO]}"></g:include>
                </g:each>
            </div>
        </div>
    </div>
    <g:include view="dashboard/popup/createTopic.gsp"></g:include>
    <g:include view="dashboard/popup/inviteUser.gsp"></g:include>
    <g:include view="dashboard/popup/createLink.gsp"></g:include>
    <g:include view="dashboard/popup/createDoc.gsp"></g:include>
    <g:include view="resource/_editPost.gsp" model="${[description:post.description,resourceId: post.id]}"></g:include>
</div>
</body>
</html>