<legend>Search Results for '${searchKey}'</legend>
<div id="ajax-post-div">
    <g:each in="${resourceDTOList}" var="topicDTO">
        <g:include view="dashboard/template/_post.gsp" model="${[post:topicDTO]}"></g:include>
    </g:each>
</div>
<div class="pagination">
    <util:remotePaginate controller="user" action="searchResourcesAjax" total="${totalRecords}" update="ajax-post-outer-div"
                         params="[searchKey:searchKey]" pageSizes="[10:'10 Per Page', 20: '20 Per Page', 50:'50 Per Page',100:'100 Per Page']"/>
</div>