<div id="edit-resource" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <g:form method="post" controller="resource" action="editPost" name="edit-resource-form" id="edit-resource-form" class="form-horizontal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Edit Post</h3>
        </div>
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label mandatory" for="description">Name</label>
                <div class="controls">
                    <textarea name="description" id="description">${description}</textarea>
                    <input type="hidden" name="resourceId" value="${resourceId}">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="submit">Save</button>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
    </g:form>
</div>