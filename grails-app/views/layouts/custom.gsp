<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 18/1/16
  Time: 3:10 PM
--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:layoutTitle/></title>
    <g:layoutHead/>
    <link rel="stylesheet" type="text/css" href="${resource(dir:"css",file: "bootstrap.min.css" )}"/>
    <asset:stylesheet src="custom.css"></asset:stylesheet>
    <asset:javascript src="jQuery.js"></asset:javascript>
    <asset:javascript src="jQuery-validator.js"></asset:javascript>
    <script type="text/javascript">
        var checkUsernameUrl = "${g.createLink(controller: 'user',action: 'isUsernameAvailable')}";
        var checkUserEmailUrl = "${g.createLink(controller: 'user',action: 'isUserEmailAvailable')}";
        var checkEmailRegisteredUrl = "${g.createLink(controller: 'user',action: 'isEmailRegistered')}";
        var validateTopicNameUrl = "${g.createLink(controller: 'topic',action: 'isTopicAvailable')}";
        var subscribeTopicUrl = "${g.createLink(controller: 'topic',action: 'topicSubscriptionChange')}";
        var readTopicUrl = "${g.createLink(controller: 'resource',action: 'toggleReadStatus')}";
        var changeTopicVisibilityUrl = "${g.createLink(controller: 'topic',action: 'topicVisibilityChange')}";
        var changeTopicSeriousnessUrl = "${g.createLink(controller: 'topic',action: 'topicSeriousnessChange')}";
        var editTopicNameUrl = "${g.createLink(controller: 'topic',action: 'topicNameEdit')}";
        var deleteTopicUrl = "${g.createLink(controller: 'topic',action: 'topicDelete')}";
        var showPostByTopicAjaxUrl = "${g.createLink(controller: 'resource',action: 'showPostByTopic')}"
        var saveResourceRatingUrl = '${g.createLink(controller: 'resource',action: 'saveResourceRating')}'
        var deletePostUrl = "${g.createLink(controller: 'resource',action: 'resourceDelete')}";
    </script>
</head>
<body>
<g:layoutBody/>
<asset:javascript src="custom.js"></asset:javascript>
<script type="text/javascript" src="${resource(dir:"js",file: "bootstrap.min.js" )}"></script>
</body>
</html>