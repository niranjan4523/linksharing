<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 23/1/16
  Time: 6:07 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${resource(dir:"css",file: "bootstrap.min.css" )}"/>
    <asset:stylesheet src="custom.css"></asset:stylesheet>
    <asset:stylesheet src="dataTable.css"></asset:stylesheet>
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>

    <div class="span12" style="margin-left: 0px;">
        <div class="row-fluid">
            <table id="userTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Active</th>
                    <th>Manage</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${userList}" var="${user}">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.username}</td>
                        <td>${user.email}</td>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td>${user.active?"Yes":"No"}</td>
                        <td>
                            <g:link controller="user" action="activateDeactivateUser" class="active"
                                    params="${[status:user.active?"deactivate":"activate"]}">
                                ${user.active?"Deactivate":"Activate"}
                            </g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>

    <g:include view="dashboard/popup/createTopic.gsp"></g:include>
    <g:include view="dashboard/popup/inviteUser.gsp"></g:include>
    <g:include view="dashboard/popup/createLink.gsp"></g:include>
    <g:include view="dashboard/popup/createDoc.gsp"></g:include>

</div>
<asset:javascript src="jQuery.js"></asset:javascript>
<asset:javascript src="jQuery-validator.js"></asset:javascript>
<asset:javascript src="jquery.dataTables.min.js"></asset:javascript>
<asset:javascript src="dataTable.js"></asset:javascript>

<script type="text/javascript" src="${resource(dir:"js",file: "bootstrap.min.js" )}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('.icon-comment').tooltip();
        $('.icon-envelope').tooltip();
        $('.icon-resize-small').tooltip();
        $('.icon-folder-close').tooltip();

        setTimeout(function(){
            $("div.alert.alert-success").hide()
            $("div.alert.alert-danger").hide()
        }, 5000);

        $('#create-topic-form').validate({
            rules:{
                topicName: {
                    required: true,
                    remote: {
                        url: "${request.contextPath}/task/isTopicAvailable",
                        type: "post",
                        data: {
                            topicName: function() {
                                return $("#create-topic-form input[name=topicName]").val().trim();
                            }
                        }
                    }
                }
            },
            messages: {
                topicName:{
                    required: "Please enter topic name.",
                    remote: "Topic name already used."
                }
            }
        });
        $('#invite-user-form').validate({
            rules:{
                inviteEmail: {
                    required: true,
                    email: true
                }
            },
            messages:{
                required: "Please enter email id.",
                email:"Please enter valid email id."
            }
        });
        $('#create-doc-form').validate({
            rules:{
                document: {required: true},
                docDesc: {required: true}
            },
            messages: {
                document: "Please select the document to upload.",
                docDesc: "Please enter document description"
            }
        });
        $('#create-link-form').validate({
            rules:{
                link: {
                    required: true,
                    url: true
                },
                linkDesc: {required: true}
            },
            messages: {
                link: {
                    required: "Please enter link to share.",
                    url: "Please enter a valid url."
                },
                linkDesc: "Please enter document description."
            }
        });
        $("a.active").click(function(){

        });
    });

</script>
</body>
</html>