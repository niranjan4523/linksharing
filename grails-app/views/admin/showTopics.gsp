<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 19/1/16
  Time: 5:29 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="custom">
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span5">
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <legend>${heading}
                    <a href="#"></a>
                </legend>
                <g:each in="${topicDTOList}" var="topicDTO">
                    <g:include view="dashboard/template/_topic.gsp" model="${[topicDTO:topicDTO,linkType:"ajax"]}"></g:include>
                </g:each>
            </div>
        </div>
        <div class="span7">
            <div class="navbar-inner" id="ajax-post-outer-div" style="margin-bottom: 20px;">
                <legend>Posts : </legend>
                <div id="ajax-post-div">
                    <label>Select the topic to show the posts.</label>
                </div>
            </div>
        </div>
    </div>
    <g:include view="dashboard/popup/createTopic.gsp"></g:include>
    <g:include view="dashboard/popup/inviteUser.gsp"></g:include>
    <g:include view="dashboard/popup/createLink.gsp"></g:include>
    <g:include view="dashboard/popup/createDoc.gsp"></g:include>
</div>
</body>
</html>