<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 19/1/16
  Time: 5:29 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<head>
    <meta name="layout" content="custom">
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span5">
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <div class="row-fluid">
                    <div class="span3 margin-vertical">
                        <img src="${user.profilePicPath}" width="100%" onerror="this.src='/images/profile/user-default.png'">
                    </div>
                    <div class="span9 margin-vertical">
                        <div class="row-fluid" style="margin-bottom: 10px">
                            <label style="font-size: 14px;margin: 0px">${user.firstName} ${user.lastName}</label>
                            <label class="gray-text">@${user.username}</label>
                        </div>
                        <div class="row-fluid" style="line-height: 5px;">
                            <div class="span6">
                                <label class="gray-text">Subscriptions</label>
                                <a href="#">${user.subscriptionCount}</a>
                            </div>
                            <div class="span6">
                                <label class="gray-text">Topics</label>
                                <a href="#">${user.topicCount}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <legend>Subscriptions
                    <g:link controller="topic" action="viewAllTopics" params="${['type':'subscribe']}">
                        <label class="pull-right" style="margin-top: 10px;font-size: 10px;">View All</label>
                    </g:link>
                </legend>
                <g:each in="${createdTopicList}" var="createdTopicDTO">
                    <g:include view="dashboard/template/_topic.gsp" model="${[topicDTO:createdTopicDTO]}"></g:include>
                </g:each>
            </div>
        </div>
        <div class="span7">
            <div class="navbar-inner" id="edit-profile-div" style="margin-bottom: 20px;">
                <legend>Profile</legend>
                <g:uploadForm method="post" controller="user" action="updateProfile" name="edit-profile-form" id="edit-profile-form" class="form-horizontal" style="margin-top: 20px;">
                    <div class="control-group">
                        <label class="control-label mandatory" for="firstName">First Name</label>
                        <div class="controls">
                            <input type="text" id="firstName" placeholder="First Name" name="firstName" value="${user.firstName}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label mandatory" for="lastName">Last Name</label>
                        <div class="controls">
                            <input type="text" id="lastName" placeholder="Last Name" name="lastName" value="${user.lastName}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label mandatory" for="username">Username</label>
                        <div class="controls">
                            <input type="text" id="username" readonly placeholder="Username" name="username" value="${user.username}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="photo">Photo</label>
                        <div class="controls">
                            <input type="file" id="photo" name="photo">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input type="submit" value="Update" class="btn btn-primary">
                        </div>
                    </div>
                </g:uploadForm>
            </div>
            <div class="navbar-inner" id="change-password-div" style="margin-bottom: 20px;">
                <legend>Change Password</legend>
                <g:form method="post" controller="user" action="changePassword" name="change-password-form" id="change-password-form" class="form-horizontal" style="margin-top: 20px;">
                    <div class="control-group">
                        <label class="control-label mandatory" for="password1">Password</label>
                        <div class="controls">
                            <input type="password" id="password1" placeholder="Password" name="password1">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label mandatory" for="password2">Confirm Password</label>
                        <div class="controls">
                            <input type="password" id="password2" placeholder="Password" name="password2">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <input type="submit" value="Update" class="btn btn-primary">
                        </div>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
    <g:include view="dashboard/popup/createTopic.gsp"></g:include>
    <g:include view="dashboard/popup/inviteUser.gsp"></g:include>
    <g:include view="dashboard/popup/createLink.gsp"></g:include>
    <g:include view="dashboard/popup/createDoc.gsp"></g:include>
</div>
</body>
</html>