<legend>Posts by ${user.firstName}&nbsp;${user.lastName}</legend>
<div id="user-post-div">
    <g:if test="${resourceDTOList}">
        <g:each in="${resourceDTOList}" var="topicDTO">
            <g:include view="dashboard/template/_post.gsp" model="${[post:topicDTO]}"></g:include>
        </g:each>
    </g:if>
    <g:else>
        <label class="gray-text">No record found.</label>
    </g:else>
</div>
<div class="pagination">
    <util:remotePaginate controller="user" action="showUserPosts" total="${totalRecords}" update="user-post-outer-div"
                         pageSizes="[10:'10 Per Page', 20: '20 Per Page', 50:'50 Per Page',100:'100 Per Page']"/>
</div>