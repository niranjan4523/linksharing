<div class="row-fluid">
    <div class="span3 margin-vertical">
        <g:link controller="user" action="showUser" params="[userId:user.id]">
            <img src="${user.profilePicPath}" width="100%" onerror="this.src='/images/profile/user-default.png'">
        </g:link>
    </div>
    <div class="span9 margin-vertical">
        <div class="row-fluid" style="margin-bottom: 10px">
            <g:link controller="user" action="showUser" params="[userId:user.id]">
                ${user.firstName} ${user.lastName}
            </g:link>
            <label class="gray-text">@${user.username}</label>
        </div>
        <div class="row-fluid" style="line-height: 5px;">
            <div class="span6">
                <label class="gray-text">Subscriptions</label>
                <a href="javascript:void(0);">${user.subscriptionCount}</a>
            </div>
            <div class="span6">
                <label class="gray-text">Topics</label>
                <a href="javascript:void(0);">${user.topicCount}</a>
            </div>
        </div>
    </div>
</div>