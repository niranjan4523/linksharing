<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 19/1/16
  Time: 5:29 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="custom">
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span12 navbar-inner">
            <legend>
                Forgot Password
            </legend>
            <label class="control-label margin">
                Please enter the registered email Id.
                A link to reset password will be sent on this email Id.
            </label>
            <g:form controller="user" action="sendResetPasswordLink" class="form-horizontal" id="forgot-password-form" name="forgot-password-form">
                <div class="control-group">
                    <label class="control-label mandatory" for="email">Email</label>
                    <div class="controls">
                        <input type="text" placeholder="Email" name="email" id="email">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function () {
    $('#forgot-password-form').validate({
        rules:{
            email: {
                required: true,
                emailCheck:true,
                remote: {
                    url: '${g.createLink(controller: 'user',action: 'isEmailRegistered')}',
                    type: "post",
                    data: {
                        email: function() {
                            return $("#forgot-password-form input[name=email]").val().trim();
                        }
                    }
                }
            }
        },
        messages: {
            email:{
                required: "Please enter email ID.",
                remote: "Email ID not registered."
            }
        }
    });
});
</script>
</body>
</html>