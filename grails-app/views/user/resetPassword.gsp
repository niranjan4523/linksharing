<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 19/1/16
  Time: 5:29 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="custom">
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span12 navbar-inner">
            <legend>
                Reset Password
            </legend>
            <g:form controller="user" action="changeResetPassword" class="form-horizontal margin" id="reset-password-form" name="reset-password-form">
                <div class="control-group">
                    <label class="control-label mandatory" for="password1">Password</label>
                    <div class="controls">
                        <input type="password" name="password1" id="password1">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label mandatory" for="password2">Confirm Password</label>
                    <div class="controls">
                        <input type="password" name="password2" id="password2">
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary">Reset Password</button>
                    </div>
                </div>
                <input type="hidden" name="userId" value="${userId}">
                <input type="hidden" name="authKey" value="${authKey}">
            </g:form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#reset-password-form').validate({
            rules:{
                password1: {
                    required: true,
                    minlength: 4
                },
                password2: {
                    required: true,
                    equalTo: "#password1"
                }
            },
            messages: {
                password1: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 4 characters long"
                },
                password2: {
                    required: "Please confirm password",
                    equalTo: "Password and Confirm password should be same"
                }
            }
        });
    });
</script>
</body>
</html>