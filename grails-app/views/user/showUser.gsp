<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 19/1/16
  Time: 5:29 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="custom">
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span5">
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <g:include view="user/_user.gsp" params="${[user:user]}"/>
            </div>
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <legend>Topics by ${user.firstName} ${user.lastName}
                    <g:if test="${userPublicTopics}">
                        <g:link controller="topic" action="viewAllTopics" params="${['type':'user','userId':user.id]}">
                            <label class="pull-right" style="margin-top: 10px;font-size: 10px;">View All</label>
                        </g:link>
                    </g:if>
                </legend>
                <g:if test="${userPublicTopics}">
                    <g:each in="${userPublicTopics}" var="topicDTO">
                        <g:include view="dashboard/template/_topic.gsp" model="${[topicDTO:topicDTO]}"></g:include>
                    </g:each>
                </g:if>
                <g:else>
                    <label class="gray-text">No record found.</label>
                </g:else>
            </div>
        </div>
        <div class="span7">
            <div class="navbar-inner" id="user-post-outer-div" style="margin-bottom: 20px;">
                <g:include view="user/_userPosts.gsp" model="${[totalRecords:totalResourceCount,resourceDTOList:userPublicResources,user: user]}"></g:include>

            </div>
        </div>
    </div>
    <g:include view="dashboard/popup/createTopic.gsp"></g:include>
    <g:include view="dashboard/popup/inviteUser.gsp"></g:include>
    <g:include view="dashboard/popup/createLink.gsp"></g:include>
    <g:include view="dashboard/popup/createDoc.gsp"></g:include>

</div>
</body>
</html>