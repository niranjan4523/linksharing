<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 18/1/16
  Time: 12:43 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="custom">
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span5">
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <legend>Trending Topics</legend>
                <g:each in="${trendingTopics}" var="trendingTopicDTO">
                    <g:include view="dashboard/template/_topic.gsp" model="${[topicDTO:trendingTopicDTO]}"></g:include>
                </g:each>
            </div>
        </div>

        <div class="span7">
            <div class="navbar-inner" id="ajax-post-outer-div" style="margin-bottom: 20px;">
                <g:include view="resource/_showSearchedPosts.gsp" model="${[totalRecords:totalRecord,resourceDTOList:listSearchResults,searchKey:searchKey]}"></g:include>
            </div>
        </div>
    </div>
</div>
</body>
</html>