<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 18/1/16
  Time: 12:43 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="custom">
</head>
<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span7">
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <legend>Recent Shares</legend>
                <g:each in="${recentShareList}" var="post">
                    <g:include view="dashboard/template/_post.gsp" model="${[post:post]}"/>
                </g:each>
            </div>

            <div class="navbar-inner" style="margin-bottom: 20px;">
                <legend>Top Posts
                    <div class="btn-group pull-right">
                        <select name="topPostView" style="margin-top: 5px;width:100px;">
                            <option value="0">Today</option>
                            <option value="1">1 Week</option>
                            <option value="2">1 Month</option>
                            <option value="3">1 Year</option>
                        </select>
                    </div>
                </legend>
                <div id="top-posts-div">
                    <g:each in="${topPosts}" var="post">
                        <g:include view="dashboard/template/_post.gsp" model="${[post:post]}"/>
                    </g:each>
                </div>
            </div>
        </div>

        <div class="span5">
            <div class="navbar-inner" id="login-div" style="margin-bottom: 20px;">
                <legend>Login</legend>
                <g:form method="post" controller="user" action="authenticate" name="login-form" id="login-form">
                    <table cellpadding="1" cellspacing="4" width="100%" class="form">
                        <tr>
                            <td>
                                <label class="mandatory">Username</label>
                            </td>
                            <td>
                                <input type="text" id="username" name="username" placeholder="Username">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mandatory">Password</label>
                            </td>
                            <td>
                                <input type="password" id="password" name="password" placeholder="Password">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="vertical-align: middle;">
                                <g:link controller="user" action="forgotPassword" style="padding-right: 20px;">Forgot Password</g:link>
                                &nbsp;&nbsp;
                                <input type="submit" value="Login" class="btn btn-info pull-right">
                            </td>
                        </tr>
                    </table>
                </g:form>
            </div>

            <div class="navbar-inner" id="register-div">
                <legend>Register</legend>

                <g:form method="post" controller="user" action="registerUser" id="register-form" name="register-form" enctype="multipart/form-data">
                    <table cellpadding="1" cellspacing="4" width="100%" class="form">
                        <tr>
                            <td>
                                <label class="mandatory">First Name</label>
                            </td>
                            <td>
                                <input type="text" name="firstName" placeholder="First Name">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mandatory">Last Name</label>
                            </td>
                            <td>
                                <input type="text" name="lastName" placeholder="Last Name">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mandatory">Email</label>
                            </td>
                            <td>
                                <input type="text" name="email" placeholder="Email">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mandatory">Username</label>
                            </td>
                            <td>
                                <input type="text" name="username" placeholder="Username">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mandatory">Password</label>
                            </td>
                            <td>
                                <input type="password" name="password1" id="password1" placeholder="Password">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="mandatory">Confirm Password</label>
                            </td>
                            <td>
                                <input type="password" name="password2" id="password2" placeholder="Confirm Password">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Photo</label>
                            </td>
                            <td><input type="file" name="photo" style="width: 206px;"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input class="btn btn-info pull-right" type="submit" value="Register">
                            </td>
                        </tr>
                    </table>
                </g:form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("select[name=topPostView]").change(function () {
        var topPostView = $(this).val();
        $.ajax({
            url: "${g.createLink(controller: 'user',action: 'showTopPosts')}",
            method: "POST",
            data: {topPostView:topPostView},
            success: function(result){
                $("#top-posts-div").html(result);
            }
        });
    });
});
</script>
</body>
</html>