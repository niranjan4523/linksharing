<div id="send-invite" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <g:form method="post" controller="topic" action="sendInviteMail" name="invite-user-form" id="invite-user-form" class="form-horizontal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Send Invitation</h3>
        </div>
        <div class="modal-body">
                <div class="control-group">
                    <label class="control-label mandatory" for="inviteEmail">Email</label>
                    <div class="controls">
                        <input type="text" id="inviteEmail" placeholder="Email" name="inviteEmail">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inviteTopic">Topic</label>
                    <div class="controls">
                        <g:select name="inviteTopic" id="inviteTopic" class="form-control"
                                  optionKey="id" optionValue="name" from="${subscribedTopics}"/>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="submit">Invite</button>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
    </g:form>
</div>