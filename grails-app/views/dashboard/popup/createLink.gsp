<div id="share-link" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <g:form method="post" controller="resource" action="createLinkResource" name="create-link-form" id="create-link-form" class="form-horizontal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Share Link</h3>
        </div>
        <div class="modal-body">
                <div class="control-group">
                    <label class="control-label mandatory" for="link">Link</label>
                    <div class="controls">
                        <input type="text" id="link" placeholder="Link" name="link">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label mandatory" for="linkDesc">Description</label>
                    <div class="controls">
                        <textarea id="linkDesc" placeholder="Description" name="linkDesc"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="linkTopic">Topic</label>
                    <div class="controls">
                        <g:select name="linkTopic" id="linkTopic"  class="form-control"
                                  optionKey="id" optionValue="name" from="${subscribedTopics}"/>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="submit">Share</button>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
    </g:form>
</div>
