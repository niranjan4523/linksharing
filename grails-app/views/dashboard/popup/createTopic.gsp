<div id="create-topic" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <g:form method="post" controller="topic" action="createTopic" name="create-topic-form" id="create-topic-form" class="form-horizontal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Create Topic</h3>
        </div>
        <div class="modal-body">
                <div class="control-group">
                    <label class="control-label mandatory" for="topicName">Name</label>
                    <div class="controls">
                        <input type="text" id="topicName" placeholder="Name" name="topicName">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="visibility">Visibility</label>
                    <div class="controls">
                        <g:select id="visibility" optionKey="value" value="name" class="form-control"
                                  name="visibility" from="${com.ttnd.linksharing.enums.Visibility.values()}" />
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" type="submit">Save</button>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
    </g:form>
</div>