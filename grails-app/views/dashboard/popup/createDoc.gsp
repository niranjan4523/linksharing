<div id="share-doc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <g:form method="post" controller="resource" action="createDocResource" name="create-doc-form" id="create-doc-form"
            class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Share Document</h3>
        </div>
        <div class="modal-body">
                <div class="control-group">
                    <label class="control-label mandatory" for="document">Document</label>
                    <div class="controls">
                        <input type="file" id="document" name="document">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label mandatory" for="docDesc">Description</label>
                    <div class="controls">
                        <textarea id="docDesc" placeholder="Description" name="docDesc"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="docTopic">Topic</label>
                    <div class="controls">
                        <g:select name="docTopic" id="docTopic" class="form-control"
                                  optionKey="id" optionValue="name" from="${subscribedTopics}"/>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary">Share</button>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
        </div>
    </g:form>
</div>