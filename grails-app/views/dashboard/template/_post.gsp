<div class="row-fluid bottom-border">
    <div class="span1 margin-vertical">
        <g:link controller="user" action="showUser" params="[userId:post.createdBy.id]">
            <img src="${post.createdBy.profilePicPath}" width="100%" onerror="this.src='/images/profile/user-default.png'">
        </g:link>
    </div>
    <div class="span11 margin-vertical">
        <div class="row-fluid">
            <g:link controller="user" action="showUser" params="[userId:post.createdBy.id]">
                ${post.createdBy.firstName}&nbsp;${post.createdBy.lastName}
            </g:link>
            <label class="gray-text" style="display: inline-block;padding: 0 20px;">@${post.createdBy.username}</label>
            <g:link controller="topic" action="viewOneTopic" params="${[topicId:post.topicId]}" class="pull-right">${post.topicName}</g:link>
        </div>
        <div class="row-fluid">
            ${post.description}
        </div>
        <div class="row-fluid">
            <g:if test="${session.user}">
                <g:link controller="resource" action="${post.linkAction}" params="${[resourceId:post.id]}" target="_blank">
                    ${post.linkText}
                </g:link>
                <a class="margin-horizontal read" event="${post.isRead?"unread":"read"}" postId="${post.id}">
                    Mark as ${post.isRead?"unread":"read"}
                </a>
            </g:if>
            <g:link controller="resource" action="viewPost" params="${[resourceId: post.id]}">View Post</g:link>
        </div>
    </div>
</div>