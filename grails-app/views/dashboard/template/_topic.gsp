<div class="bottom-border topicDiv" topicId="${topicDTO.id}">
<div class="row-fluid margin-vertical">
    <div class="row-fluid">
        <div class="span2">
            <g:link controller="user" action="showUser" params="[userId:topicDTO.createdBy.id]">
                <img src="${topicDTO.createdBy.profilePicPath}" width="100%"
                     onerror="this.src = '/images/profile/user-default.png'">
            </g:link>
        </div>

        <div class="span10">
            <div class="row-fluid topicNameDisplay">
                <g:if test="${linkType}">
                    <a href="javascript:void(0);" class="showPostAjax">
                        ${topicDTO.name}
                    </a>
                </g:if>
                <g:else>
                    <g:link controller="topic" action="viewOneTopic" params="${[topicId: topicDTO.id]}">
                            ${topicDTO.name}
                    </g:link>
                </g:else>
            </div>

            <div class="row-fluid topicNameEdit" style="display: none;">
                <input type="text" name="topicName" value="${topicDTO.name}">
                <a href="javascript:void(0);" class="saveTopicName">
                    <i class="icon-ok" title="Save" data-placement="bottom"></i>
                </a>
                <a href="javascript:void(0);" class="cancelTopicName">
                    <i class="icon-remove" title="Cancel" data-placement="bottom"></i>
                </a>
            </div>

            <div class="row-fluid">
                <div class="span4">
                    <label class="gray-text">@${topicDTO.createdBy.username}</label>
                    <g:if test="${session.user}">
                        <g:if test="${topicDTO.createdBy.id == session.user.id}">
                            <label class="gray-text">Subscribed</label>
                        </g:if>
                        <g:else>
                            <g:if test="${topicDTO.loggedInUserSeriousness}">
                                <a class="subscribe" event="unsubscribe">
                                    Unsubscribe
                                </a>
                            </g:if>
                            <g:else>
                                <a class="subscribe" event="subscribe">
                                    Subscribe
                                </a>
                            </g:else>
                        </g:else>
                    </g:if>
                </div>

                <div class="span4">
                    <label class="gray-text">Subscriptions</label>
                    <g:link controller="topic" action="viewOneTopic" params="${[topicId: topicDTO.id]}">
                        ${topicDTO.subscriberCount}
                    </g:link>
                </div>

                <div class="span4">
                    <label class="gray-text">Posts</label>
                    <g:link controller="topic" action="viewOneTopic" params="${[topicId: topicDTO.id]}">
                        ${topicDTO.resourceCount}
                    </g:link>
                </div>
            </div>
            <g:if test="${session.user && (session.user.admin || (topicDTO.createdBy.id == session.user.id))}">
                <div class="row-fluid">
                    <g:select optionKey="value" value="${topicDTO.visibility.value}"
                              class="form-control topicVisibility" style="width: auto;"
                              name="visibility" from="${com.ttnd.linksharing.enums.Visibility.values()}"/>
                    <g:if test="${topicDTO.loggedInUserSeriousness}">
                        <g:select optionKey="value" value="${topicDTO.loggedInUserSeriousness.value}"
                                  class="form-control topicSeriousness" style="width: auto;"
                                  name="seriousness" from="${com.ttnd.linksharing.enums.Seriousness.values()}"/>
                        <a href="#send-invite" data-toggle="modal" class="sendInviteSingle">
                            <i class="icon-envelope" title="Send Invite" data-placement="bottom"></i>
                        </a>
                    </g:if>
                    <a href="javascript:void(0);" class="editTopic">
                        <i class="icon-edit" title="Edit topic" data-placement="bottom"></i>
                    </a>
                    <a href="javascript:void(0);" class="deleteTopic">
                        <i class="icon-trash" title="Delete topic" data-placement="bottom"></i>
                    </a>
                </div>
            </g:if>
            <g:else>
                <div class="row-fluid">
                <g:if test="${topicDTO.loggedInUserSeriousness}">
                    <g:select optionKey="value" value="${topicDTO.loggedInUserSeriousness.value}"
                              class="form-control topicSeriousness" style="width: auto;"
                              name="seriousness" from="${com.ttnd.linksharing.enums.Seriousness.values()}"/>
                    <a href="#send-invite" data-toggle="modal">
                        <i class="icon-envelope" title="Send Invite" data-placement="bottom"></i>
                    </a>
                </g:if>
                </div>
            </g:else>
        </div>
    </div>

    <div class="row-fluid">
        <div class="alert-success row-fluid text-center" style="display: none;font-size: 10px;">
        </div>

        <div class="alert-danger row-fluid text-center" style="display: none;font-size: 10px;">
        </div>
    </div>
</div>
</div>
