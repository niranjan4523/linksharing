<legend>Inbox</legend>
<div id="ajax-inbox-div">
    <g:each in="${resourceDTOList}" var="topicDTO">
        <g:include view="dashboard/template/_post.gsp" model="${[post:topicDTO]}"></g:include>
    </g:each>
</div>
<div class="pagination">
    <util:remotePaginate controller="dashboard" action="showInboxPosts" total="${totalRecords}" update="inbox-outer-div"
                         pageSizes="[10:'10 Per Page', 20: '20 Per Page', 50:'50 Per Page',100:'100 Per Page']"/>
</div>