<%--
  Created by IntelliJ IDEA.
  User: niranjan
  Date: 23/1/16
  Time: 6:07 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="custom">
</head>

<body>
<div class="container" style="margin-bottom: 20px;">
    <g:include view="common/_header.gsp"></g:include>
    <g:include view="common/_message.gsp"></g:include>
    <div class="row-fluid">
        <div class="span5">
            <div class="navbar-inner" style="margin-bottom: 20px;">
                <legend>Topic : "${topic.name}"</legend>
                <g:include view="dashboard/template/_topic.gsp" model="${[topicDTO: topic]}"></g:include>
            </div>

            <div class="navbar-inner" style="margin-bottom: 20px;">
                <legend><b>Users :</b>"${topic.name}"</legend>
                <g:each in="${topic.subscriberList}" var="user">
                    <div class="row-fluid bottom-border margin-vertical">
                        <div class="span2 margin-vertical">
                            <g:link controller="user" action="showUser" params="[userId: user.id]">
                                <img src="${user.profilePicPath}" width="100%"
                                     onerror="this.src = '/images/profile/user-default.png'">
                            </g:link>
                        </div>

                        <div class="span10 margin-vertical">
                            <div class="row-fluid" style="margin-bottom: 10px">
                                <g:link controller="user" action="showUser" params="[userId: user.id]">
                                    ${user.firstName} ${user.lastName}
                                </g:link>
                                <label class="gray-text">@${user.username}</label>
                            </div>

                            <div class="row-fluid" style="line-height: 5px;">
                                <div class="span6">
                                    <label class="gray-text">Subscriptions</label>
                                    <label class="gray-text">
                                        ${user.subscriptionCount}
                                    </label>
                                </div>

                                <div class="span6">
                                    <label class="gray-text">Topics</label>
                                    <label class="gray-text">
                                        ${user.topicCount}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </g:each>
            </div>
        </div>

        <div class="span7">
            <div class="navbar-inner" id="ajax-post-outer-div" style="margin-bottom: 20px;">
                <g:include view="resource/_showPostByTopic.gsp"
                           model="${[totalRecords: totalResources, resourceDTOList: topic.resourceList, topicId: topic.id]}"></g:include>
            </div>
        </div>
    </div>
    <g:include view="dashboard/popup/createTopic.gsp"></g:include>
    <g:include view="dashboard/popup/inviteUser.gsp"></g:include>
    <g:include view="dashboard/popup/createLink.gsp"></g:include>
    <g:include view="dashboard/popup/createDoc.gsp"></g:include>
</div>
</body>
</html>