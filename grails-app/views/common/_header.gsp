<div class="navbar" style="margin-top: 5px;">
    <div class="navbar-inner">
        <g:if test="${session.user}">
            <g:link controller="dashboard" action="index" class="brand">Link Sharing</g:link>
        </g:if>
        <g:else>
            <g:link controller="user" action="login" class="brand">Link Sharing</g:link>
        </g:else>
        <div class="pull-right">
            <g:form name="search-form" controller="user" action="searchResources" method="post" class="navbar-search form-search">
                <div class="input-append">
                    <input type="text" class="span2 search-query" placeholder="Search" name="searchKey" value="${searchKey}">
                    <a href="javascript:void(0);" class="btn" id="searchButton"><i class="icon-search"></i></a>
                </div>
            </g:form>
            <form class="navbar-search form-search" style="margin-top: 8px;">
                <g:if test="${session.user}">
                    <a href="#create-topic" data-toggle="modal" style="margin-left: 20px;"><i class="icon-comment" title="Create Topic" data-placement="bottom"></i></a>
                    <a href="#send-invite" data-toggle="modal"><i class="icon-envelope" title="Send Invite" data-placement="bottom"></i></a>
                    <a href="#share-link" data-toggle="modal"><i class="icon-resize-small" title="Share Link" data-placement="bottom"></i></a>
                    <a href="#share-doc" data-toggle="modal"><i class="icon-folder-close" title="Share Resource" data-placement="bottom"></i></a>
                    <a href="#my-profile" data-toggle="modal"><i class="icon-user" style="margin-left: 20px;"></i></a>
                    <div class="pull-right" style="margin-top: 0px;margin-left: 5px;">
                        <button data-toggle="dropdown" class="btn btn-small dropdown-toggle" style="margin-top: 0px;">
                            ${session.user.firstName}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" style="min-width: 100px;">
                            <li><g:link controller="user" action="editProfile">Profile</g:link></li>
                            <g:if test="${session.user.admin}">
                                <li><g:link controller="admin" action="showUserList">users</g:link></li>
                                <li><g:link controller="admin" action="showTopics">Topics</g:link></li>
                                <li><g:link controller="admin" action="showPosts">Posts</g:link></li>
                            </g:if>
                            <li><g:link controller="user" action="logout">Logout</g:link></li>
                        </ul>
                    </div>
                </g:if>
            </form>
        </div>
    </div>
</div>