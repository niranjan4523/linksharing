package linksharing

import com.google.gson.JsonObject
import com.ttnd.linksharing.Subscription
import com.ttnd.linksharing.co.DocResourceCO
import com.ttnd.linksharing.co.LinkResourceCO
import com.ttnd.linksharing.dto.ResourceDTO
import com.ttnd.linksharing.ReadingItem
import com.ttnd.linksharing.Topic
import com.ttnd.linksharing.User
import com.ttnd.linksharing.enums.Visibility
import com.ttnd.linksharing.resource.DocumentResource
import com.ttnd.linksharing.resource.LinkResource
import com.ttnd.linksharing.resource.Resource
import com.ttnd.linksharing.resource.ResourceRating
import grails.transaction.Transactional

@Transactional
class ResourceService {

    TopicService topicService;

    List<ResourceDTO> getRecentlySharedPosts() {
        List<Resource> resourceList = Resource.createCriteria().list() {
            topic {
                eq('visibility', com.ttnd.linksharing.enums.Visibility.PUBLIC)
            }
            firstResult(0)
            maxResults(5)
            order('id', 'desc')
        }
        return createResourceDTOList(resourceList, null);
    }

    List<ResourceDTO> getTopPosts(Date date) {

        List<Object[]> resourceDataList = ResourceRating.createCriteria().list {
            projections {
                property('resource')
                avg('rating', 'avgRating')
            }
            resource {
                topic {
                    eq('visibility', com.ttnd.linksharing.enums.Visibility.PUBLIC)
                }
            }
            ge('lastUpdated', date)
            groupProperty('resource')
            maxResults(5)
            order('avgRating', 'desc')
        }

        List<Resource> resourceList = [];

        resourceDataList.each {
            resourceList.add(it[0]);
        }
        return createResourceDTOList(resourceList, null);
    }

    long getTotalSearchResultsCount(String searchKey, User user) {
        List allowedTopicList = allowedTopicList(user)
        long count = Resource.createCriteria().count() {
            inList('topic',allowedTopicList);
            or {
                topic {
                    ilike('name', "%${searchKey}%")
                }
                ilike('description', "%${searchKey}%")
            }
        }
        return count;
    }

    List<ResourceDTO> searchResources(String searchKey, User user,int offset,int max) {
        List allowedTopicList = allowedTopicList(user)
        List<Resource> resourceList = Resource.createCriteria().list {
            inList('topic',allowedTopicList);
            or {
                topic {
                    ilike('name', "%${searchKey}%")
                }
                ilike('description', "%${searchKey}%")
                firstResult(offset);
                maxResults(max);
            }
        }
        return createResourceDTOList(resourceList, user);
    }

    List allowedTopicList(User user) {
        List allowedTopicList = [];
        if (user && user.admin) {
            allowedTopicList = Topic.createCriteria().list {}
        } else if (user) {
            allowedTopicList = Subscription.createCriteria().list {
                projections {
                    property('topic')
                }
                or {
                    topic {
                        eq('visibility', Visibility.PUBLIC)
                    }
                    eq('user', user)
                }
            }
        } else {
            allowedTopicList = Topic.createCriteria().list {
                eq('visibility', Visibility.PUBLIC)
            }
        }

        return allowedTopicList;
    }

    def createLinkResource(LinkResourceCO resourceCO) {
        Topic topic = Topic.findById(resourceCO.linkTopic);
        Resource resource = new LinkResource(url: resourceCO.link, description: resourceCO.linkDesc,
                createdBy: resourceCO.user, topic: topic);
        resourceCO.topicName = topic.name;//For displaying that in which topic the resource is created
        resource.save(flush: true);
        markUnreadForAllUsers(resource)
    }

    def createDocResource(DocResourceCO resourceCO) {
        Topic topic = Topic.findById(resourceCO.docTopic);
        Resource resource = new DocumentResource(filePath: "temp", description: resourceCO.docDesc,
                createdBy: resourceCO.user, topic: topic);
        resource.save();
        resourceCO.uploadDoc(resource.id)
        resourceCO.topicName = topic.name;
        resource.filePath = resourceCO.fileLocation
        resource.save(flush: true)
        markUnreadForAllUsers(resource)
    }

    def markUnreadForAllUsers(Resource resource) {
        resource.topic.subscriber.each {
            ReadingItem readingItem = new ReadingItem();
            readingItem.user = it.user;
            readingItem.resource = resource;
            readingItem.isRead = false;
            readingItem.save()
        }
    }

    DocumentResource getDocResource(long resourceId) {
        Resource.findById(resourceId)
    }

    LinkResource getLinkResource(long resourceId) {
        Resource.findById(resourceId)
    }

    String resourceReadChange(long userId, long resourceId, String event) {
        println("userId :: ${userId}\npostId :: ${resourceId}\nevent :: ${event}")
        ReadingItem item = ReadingItem.findOrCreateByUserAndResource(User.load(userId), Resource.load(resourceId));
        if (item) {
            item.isRead = "read".equals(event);
            item.save(flush: true)
            return "done"
        } else {
            return "error"
        }
    }

    String getTopicName(long topicId) {
        topicService.getOneTopic(topicId).name
    }

    long getTotalResourceCount(long topicId) {
        return Resource.countByTopic(Topic.load(topicId));
    }

    List<ResourceDTO> getTopicResourceList(long topicId, User loggedUser, int offset, int max) {
        List<Resource> resourceList = Resource.createCriteria().list([offset: offset, max: max]) {
            eq('topic.id', topicId)
        }
        return createResourceDTOList(resourceList, loggedUser);
    }

    List<ResourceDTO> createResourceDTOList(List<Resource> resourceList, User loggedUser) {
        List<ResourceDTO> resourceDTOList = new ArrayList<>();
        List<Object[]> resourceReadingList = [];
        if (loggedUser) {
            resourceReadingList = ReadingItem.executeQuery("Select r.id,r.isRead from ReadingItem r " +
                    "where r.user.id=${loggedUser.id}");
        }
        Map<Long, ArrayList> resourceDataMap = getResourceTopicDataMap();
        Map resourceReadingMap = [:]
        resourceReadingList.each {
            resourceReadingMap.put(it[0], it[1])
        }
        resourceList.each { resource ->
            resourceDTOList.add(createResourceDTO(resource, resourceReadingMap, resourceDataMap.get(resource.id)))
        }
        return resourceDTOList;
    }

    Map<Long, ArrayList> getResourceTopicDataMap() {
        List<Object[]> resourceDataList = Resource.createCriteria().list {
            projections {
                property('id')
                'topic' {
                    property('id')
                    property('name')
                }
            }
        }

        Map<Long, ArrayList> resourceDataMap = [:]
        List topicDataList = []
        resourceDataList.each {
            topicDataList = new ArrayList();
            topicDataList.add(it[1]);
            topicDataList.add(it[2]);
            resourceDataMap.put(it[0], topicDataList);
        }

        return resourceDataMap;
    }

    ResourceDTO createResourceDTO(Resource resource, Map resourceReadingMap, ArrayList resourceDataList) {
        println("resourceDataList :::::: ${resourceDataList}")
        ResourceDTO resourceDTO = new ResourceDTO(id: resource.id, description: resource.description, topic: resource.topic, createdBy: resource.createdBy)

        if (resource instanceof LinkResource) {
            resourceDTO.linkAction = "visitUrl";
            resourceDTO.linkText = "Visit Site"
        } else {
            resourceDTO.linkAction = "downloadFile";
            resourceDTO.linkText = "Download"
        }
        if (resourceReadingMap.get(resource.id)) {
            resourceDTO.isRead = true;
        }
        resourceDTO.topicId = resourceDataList.get(0);
        resourceDTO.topicName = resourceDataList.get(1);
        return resourceDTO;
    }

    List<ResourceDTO> getUnreadResourceList(User user, int offset, int limit) {
        List<Resource> unreadResourceList = ReadingItem.createCriteria().list {
            projections {
                property('resource')
            }
            eq('user', user)
            eq('isRead', false)
            firstResult(offset)
            setMaxResults(limit)
            order('id','desc')
        }
        return createResourceDTOList(unreadResourceList, user);
    }

    int getTotalUnreadResourceCount(User user) {
        return ReadingItem.countByUserAndIsRead(user, false);
    }

    ResourceDTO getResource(long resourceId, User user) {
        Resource resource = Resource.findById(resourceId);
        ResourceDTO resourceDTO = new ResourceDTO(id: resource.id, createdBy: resource.createdBy, description: resource.description)
        resourceDTO.topicId = resource.topic.id;
        resourceDTO.topicName = resource.topic.name;
        if (resource instanceof LinkResource) {
            resourceDTO.linkAction = "visitUrl";
            resourceDTO.linkText = "Visit Site"
        } else {
            resourceDTO.linkAction = "downloadFile";
            resourceDTO.linkText = "Download"
        }
        resourceDTO.dateCreated = resource.dateCreated;
        ResourceRating rating = ResourceRating.findByResourceAndUser(resource, user)
        if (rating) {
            resourceDTO.rating = rating.rating;
        }
        return resourceDTO;
    }

    JsonObject saveResourceRating(long resourceId, int score, User user) {
        JsonObject jsonObject = new JsonObject();
        if (resourceId && score) {
            ResourceRating rating = ResourceRating.findByResourceAndUser(Resource.load(resourceId), user);
            if (rating) {
                rating.rating = score;
            } else {
                rating = new ResourceRating(resource: Resource.load(resourceId), user: user, rating: score);
            }
            rating.save(flush: true);
            jsonObject.addProperty("success", "Ratings saved.");
        } else {
            jsonObject.addProperty("error", "Error saving ratings.");
        }
        return jsonObject;
    }

    int getResourceRating(long resourceId, User user) {
        ResourceRating rating = ResourceRating.findByResourceAndUser(Resource.load(resourceId), user);
        if (rating) {
            return rating.rating;
        } else {
            return 0;
        }
    }

    String deleteResource(long resourceId, User user) {
        Resource resource = Resource.findById(resourceId);
        if (resource) {
            if (resource.createdBy.id == user.id || user.admin) {
                resource.delete();
                return "";
            } else {
                return "Permission denied to delete post.";
            }
        } else {
            return "Post does not exists."
        }
    }

    String editResource(long resourceId,String description,User user) {
        Resource resource = Resource.findById(resourceId);
        if (resource) {
            if (resource.createdBy.id == user.id || user.admin) {
                resource.description =description;
                resource.save(flush: true);
                return "";
            } else {
                return "Permission denied to edit post.";
            }
        } else {
            return "Post does not exists."
        }
    }
}
