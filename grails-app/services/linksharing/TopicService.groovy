package linksharing

import com.google.gson.JsonObject
import com.ttnd.linksharing.co.TopicCO
import com.ttnd.linksharing.constants.Constant
import com.ttnd.linksharing.dto.ResourceDTO
import com.ttnd.linksharing.dto.TopicDTO
import com.ttnd.linksharing.dto.UserDTO
import com.ttnd.linksharing.enums.Seriousness
import com.ttnd.linksharing.enums.Visibility
import com.ttnd.linksharing.Invitation
import com.ttnd.linksharing.Subscription
import com.ttnd.linksharing.Topic
import com.ttnd.linksharing.User
import com.ttnd.linksharing.resource.Resource
import com.ttnd.linksharing.resource.ResourceRating
import com.ttnd.linksharing.util.CommonUtils
import grails.plugin.mail.MailService
import grails.transaction.Transactional

@Transactional
class TopicService {

    ResourceService resourceService;
    MailService mailService;

    Topic getOneTopic(long topicId) {
        Topic.findById(topicId)
    }

    Topic findTopicByName(String name) {
        Topic.findByName(name)
    }

    Topic createTopic(TopicCO topicCO) {
        Topic topic = new Topic(name: topicCO.topicName, createdBy: topicCO.user,
                visibility: Visibility.getVisibilityEnum(topicCO.visibility));
        return saveTopicAndSubscribe(topicCO.user, topic, Seriousness.VERY_SERIOUS);
    }

    Topic saveTopicAndSubscribe(User user, Topic topic, Seriousness seriousness) {
        Subscription subscription = new Subscription(user: user, seriousness: seriousness);
        topic.addToSubscriber(subscription);
        return topic.save(flush: true);
    }

    TopicDTO getOneTopic(long topicId, User loggedUser) {
        Topic topic = Topic.findById(topicId);
        List<UserDTO> subscribedUserList = getSubscribedUserList(topicId);
        List<ResourceDTO> resourceList = resourceService.getTopicResourceList(topicId, loggedUser, 0, 10);
        TopicDTO topicDTO = new TopicDTO(id: topic.id, name: topic.name, visibility: topic.visibility, createdBy: topic.createdBy,
                subscriberList: subscribedUserList, resourceList: resourceList)
        topicDTO.subscriberCount = Subscription.countByTopic(topic);
        topicDTO.resourceCount = Resource.countByTopic(topic);
        return topicDTO;

    }

    List<UserDTO> getSubscribedUserList(long topicId) {
        List<User> userList = Subscription.createCriteria().list() {
            projections {
                property('user')
            }
            'topic' {
                eq('id', topicId)
            }
        }
        List<UserDTO> userDTOList = new ArrayList<>();
        UserDTO userDTO;
        userList.each { user ->
            userDTO = new UserDTO(id: user.id, username: user.username, firstName: user.firstName, lastName: user.lastName,
                    profilePicPath: user.profilePicPath, admin: user.admin, active: user.active);
            userDTO.subscriptionCount = getTopicCount(user, "subscribed");
            userDTO.topicCount = getTopicCount(user, "created");
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    int getTopicCount(User user, String type) {
        if ("created".equals(type)) {
            Topic.countByCreatedBy(user)
        } else if ("subscribed".equals(type)) {
            Subscription.countByUser(user)
        }
    }

    int getTotalTopicCount(User user, String type) {
        int totalTopicCount;
        if (user.admin) {
            totalTopicCount = Topic.count();
        } else if ("subscribed".equals(type)) {
            totalTopicCount = Subscription.createCriteria().count{
                eq('user.id',user.id)
            }
        } else if ("created".equals(type)) {
            totalTopicCount = Topic.countByCreatedBy(user);
        }
        return totalTopicCount;
    }

    List<TopicDTO> getAllTopicList(User user, int offset, int max) {
        List<Topic> topicList = Topic.createCriteria().list {
            firstResult(offset)
            maxResults(max)
        }
        return createTopicDTO(topicList, getSubscribersCountMap(), user)
    }

    List<TopicDTO> getSubscribedTopicList(User user, int offset, int max) {
        List<Topic> topicList = Subscription.createCriteria().list{
            projections{
                property('topic')
            }
            eq('user',user)
            firstResult(offset)
            maxResults(max)
            topic {
                order('name')
            }
        }
        return createTopicDTO(topicList, getSubscribersCountMap(), user)
    }

    List<TopicDTO> getTrendingTopicList(User user, int offset, int max) {
        List<Object[]> topicDataList = ResourceRating.createCriteria().list {
            projections {
                resource{
                    property('topic')
                }
                avg('rating', 'avgRating')
            }
            resource {
                topic{
                    eq('visibility',Visibility.PUBLIC)
                }
            }
            ge('lastUpdated', new Date()-30)
            resource{
                groupProperty('topic')
            }
            firstResult(offset)
            maxResults(max)
            order('avgRating', 'desc')
        }

        List<Topic> topicList = [];

        topicDataList.each {
            topicList.add(it[0]);
        }
        return createTopicDTO(topicList, getSubscribersCountMap(), user)
    }

    List<TopicDTO> getCreatedTopicList(User user, int offset, int max) {
        List<Topic> topicList = Resource.executeQuery("select t from Topic t where t.createdBy=:user order by t.id desc", ['user': user], [max: max, offset: offset])
        return createTopicDTO(topicList, getSubscribersCountMap(), user)
    }

    Map getSubscribersCountMap() {
        List<Object[]> subscriptionCountList = Subscription.executeQuery("select s.topic.id,count(s.topic.id) as subscriberCount from Subscription s group by s.topic.id")
        Map subscribersCountMap = [:]
        subscriptionCountList.each {
            subscribersCountMap.put(it[0], it[1])
        }
        return subscribersCountMap;
    }

    List<TopicDTO> createTopicDTO(List<Topic> topicList, Map subscribersCountMap, User user) {
        List<TopicDTO> topicDTOList = new ArrayList<>();
        TopicDTO topicDTO;
        if (subscribersCountMap.size() > 0) {
            List<Object[]> resourceCountList = Resource.executeQuery("select r.topic.id,count(r.topic.id) as resourceCount from Resource r where r.topic.id in (:topicIds) group by r.topic.id", ['topicIds': subscribersCountMap.keySet()])
            Map resourcesCountMap = [:]
            resourceCountList.each {
                resourcesCountMap.put(it[0], it[1])
            }

            List<Object[]> seriousnessList = []
            if(user) {
                seriousnessList = Subscription.executeQuery("select s.topic.id,s.seriousness from Subscription s where s.topic.id in (:topicIds) and s.user=:user", ['topicIds': subscribersCountMap.keySet(), 'user': user])
            }
            Map seriousnessMap = [:]
            seriousnessList.each {
                seriousnessMap.put(it[0], it[1])
            }

            topicList.each { topic ->
                topicDTO = new TopicDTO(id: topic.id, name: topic.name, visibility: topic.visibility, createdBy: topic.createdBy);
                topicDTO.subscriberCount = subscribersCountMap.get(topic.id) ? subscribersCountMap.get(topic.id) : 0;
                topicDTO.resourceCount = resourcesCountMap.get(topic.id) ? resourcesCountMap.get(topic.id) : 0;
                topicDTO.loggedInUserSeriousness = seriousnessMap.get(topic.id) ? seriousnessMap.get(topic.id) : null;
                topicDTOList.add(topicDTO);
            }
        }
        return topicDTOList;
    }

    JsonObject topicSubscriptionChange(long topicId, User user, String event) {
        Subscription subscription;
        JsonObject jsonObject = new JsonObject();
        Topic topic = Topic.findById(topicId);
        if (topic) {
            subscription = Subscription.findByTopicAndUser(Topic.load(topicId), user)
            if ("subscribe".equals(event)) {
                if (subscription) {
                    jsonObject.addProperty("success", "Topic already subscribed.");
                } else {
                    subscription = new Subscription(topic: Topic.load(topicId), user: user, seriousness: Seriousness.VERY_SERIOUS);
                    subscription.save()
                    jsonObject.addProperty("success", "Topic subscribed.");
                }
            } else {
                if (subscription) {
                    subscription = Subscription.findByTopicAndUser(Topic.load(topicId), user);
                    subscription.delete();
                    jsonObject.addProperty("success", "Topic unsubscribed.");
                } else {
                    jsonObject.addProperty("success", "Topic already unsubscribed.");
                }
            }
        } else {
            jsonObject.addProperty("error", "Topic does not exists.");
        }
        return jsonObject;
    }

    JsonObject topicVisibilityChange(long topicId, int visibility) {
        Topic topic = Topic.findById(topicId);
        JsonObject jsonObject = new JsonObject();
        if (topic) {
            topic.visibility = Visibility.getVisibilityEnum(visibility);
            topic.save(flush: true);
            jsonObject.addProperty("success", "Visibility changed to " + Visibility.getVisibilityEnum(visibility) + ".");
        } else {
            jsonObject.addProperty("error", "Topic does not exists.")
        }
        return jsonObject;
    }

    JsonObject topicSeriousnessChange(long topicId, User user, int seriousness) {
        Subscription subscription = Subscription.findByTopicAndUser(Topic.load(topicId), user);
        JsonObject jsonObject = new JsonObject();
        if (subscription) {
            subscription.seriousness = Seriousness.getSeriousness(seriousness)
            subscription.save(flush: true)
            jsonObject.addProperty("success", "Seriousness set to ${Seriousness.getSeriousness(seriousness)}.")
        }
        jsonObject.addProperty("error", "Please subscribe to topic first.")
        return jsonObject;

    }

    String topicInviteUser(long topicId, User user, String email) {

    }

    String checkForOtherInvites(String inviteEmail,long inviteTopic) {
        User user = User.findByEmail(inviteEmail);
        Subscription subscription = Subscription.findByTopicAndUser(Topic.load(inviteTopic),user);
        if(subscription) {
            return "User already subscribed to this topic."
        } else {
            Invitation invitation = Invitation.findByInvitedUserAndTopic(user,Topic.load(inviteTopic));
            if(invitation) {
                return "Invitation already sent."
            } else {
                return ""
            }
        }
    }

    String sendTopicInviteMail(String inviteEmail,long inviteTopic,User user) {
        User invitedUser = User.findByEmail(inviteEmail);
        User invitedBy = user;
        Topic topic = Topic.load(inviteTopic);
        String authenticationKey = CommonUtils.randomAlphaNumeric(30);
        Invitation invitation = new Invitation(invitedUser: invitedUser,invitedBy: invitedBy,topic: topic,authenticationKey: authenticationKey);
        invitation.save(flush: true);
        createAndSendInviteMail(invitation,inviteEmail);
    }

    Invitation createAndSendInviteMail(Invitation invitation,String toEmail) {
        User invitedBy = invitation.invitedBy;
        mailService.sendMail {
            to toEmail
            subject "LinkSharing Topic Subscription Invitation"
            html "Thank you for being on ${Constant.DOMAIL_URL},<br>" +
                    "<br>" +
                    "<b>${invitedBy.firstName+" "+invitedBy.lastName}</b>&nbsp;has invited you to subscribe topic&nbsp;" +
                    "<b>${invitation.topic.name}</b>" +
                    "<br>" +
                    "Please click the link below to subscribe topic.<br>" +
                    "<br>" +
                    "${Constant.DOMAIL_URL}/topic/acceptInvite?id=${invitation.id}&authKey=${invitation.authenticationKey}<br>" +
                    "<br>" +
                    "Regards<br>" +
                    "Link Sharing Administration"
        }
        return invitation
    }

    JsonObject topicNameEdit(long topicId, String topicName, User user) {
        Topic topic = Topic.findById(topicId);
        Topic topic1 = Topic.findByNameAndCreatedBy(topicName, user);
        JsonObject jsonObject = new JsonObject();
        if (topic1) {
            jsonObject.addProperty("error", "Topic Name already exists.")
        } else if (topic) {
            topic.name = topicName
            topic.save(flush: true)
            jsonObject.addProperty("success", "Topic Name changed to ${topicName}")
        } else {
            jsonObject.add("error", "Topic does not exists.")
        }
        return jsonObject;
    }

    String topicDelete(long topicId) {
        Topic topic = Topic.findById(topicId);
        if (topic) {
            topic.delete(flush: true);
            return "Topic deleted successfully."
        } else {
            return "Topic does not exists."
        }
    }

    String getUserFullName(long userId) {
        User user = User.findById(userId);
        return user.firstName+" "+user.lastName;
    }

    Invitation acceptInvite(long id,String authKey) {
        Invitation invitation = Invitation.findByIdAndAuthenticationKey(id,authKey);
        if(invitation) {
            saveTopicAndSubscribe(invitation.invitedUser,invitation.topic,Seriousness.VERY_SERIOUS);
        }
        return invitation;
    }
}
