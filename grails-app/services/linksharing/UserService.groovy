package linksharing

import com.ttnd.linksharing.co.LoginCO
import com.ttnd.linksharing.co.RegisterUserCO
import com.ttnd.linksharing.constants.Constant
import com.ttnd.linksharing.dto.ResourceDTO
import com.ttnd.linksharing.dto.TopicDTO
import com.ttnd.linksharing.dto.UserDTO
import com.ttnd.linksharing.enums.Visibility
import com.ttnd.linksharing.Topic
import com.ttnd.linksharing.User
import com.ttnd.linksharing.resource.Resource
import com.ttnd.linksharing.util.CommonUtils
import com.ttnd.linksharing.util.SendActivationMail
import grails.plugin.mail.MailService
import grails.transaction.Transactional

@Transactional
class UserService {

    TopicService topicService;
    ResourceService resourceService;
    MailService mailService;

    User createAdminUser() {
        if(!User.first()) {
            User adminUser = new User(firstName: "LinkSharing", lastName: "Administrator", username: "admin",
                    password: CommonUtils.generateMd5("admin"), verified: true, active: true, admin: true, email: "admin@linksharing.com");
            adminUser.save(flush: true);
        }
    }

    User registerUser(RegisterUserCO registerUserCO) {
        User user=new User(firstName: registerUserCO.firstName,lastName: registerUserCO.lastName,
                email: registerUserCO.email,username:registerUserCO.username,authenticationKey: registerUserCO.authenticationKey,
                password: registerUserCO.password1,admin: false,active: false)
        user.save()
        if(registerUserCO.photo) {
            registerUserCO.uploadPhoto(user.id)
        }
        user.profilePicPath = registerUserCO.fileLocation;
        sendActivationMail(user)
        user.save(flush: true)
    }

    void sendActivationMail(User user) {
        def sendMailClosure = {User mailUser->
            mailService.sendMail {
                to mailUser.email
                subject "LinkSharing Account Activation"
                body "Thank you for registering on ${Constant.DOMAIL_URL},\n" +
                        "\n" +
                        "Please click the link below to activate your account.\n" +
                        "\n" +
                        "${Constant.DOMAIL_URL}/user/verifyAndActivateUser?userId=${mailUser.id}&authKey=${mailUser.authenticationKey}\n" +
                        "\n" +
                        "Regards\n" +
                        "Link Sharing Administration"

            }
        }
        SendActivationMail sendActivationMail = new SendActivationMail(sendMailClosure,user);
        sendActivationMail.start();

    }

    Map verifyAndActivateUser(long userId,String authKey) {
        Map messageMap = [:];
        User user = User.findById(userId);
        if(user) {
            if(user.verified) {
                if(user.active) {
                    messageMap.message="User login already verified.";
                } else {
                    messageMap.error="User login is deactivated, please contact Admin.";
                }
            } else {
                if(authKey.equals(user.authenticationKey)) {
                    user.verified = true;
                    user.active = true;
                    user.authenticationKey = ""
                    user.save(flush: true);
                    messageMap.message="User successfully verified.";
                } else {
                    messageMap.error="Invalid/Old url for verification, please contact Admin.";
                }
            }
        } else {
            messageMap.error="User does not exists.";
        }
        return messageMap;
    }

    String sendResetPasswordLink(String email) {
        User user = User.findByEmail(email);
        if(user) {
            user.authenticationKey = CommonUtils.randomAlphaNumeric(30);
            if(user.verified) {
                sendResetPasswordMail(user)
                return "";
            } else {
                sendActivationMail(user)
                return "User is still not verified. We have sent a new mail for Account activation."
            }
        } else {
            return "Email Id is not registered with us."
        }
    }

    void sendResetPasswordMail(User user) {
        mailService.sendMail {
            to user.email
            subject "LinkSharing Password Reset"
            html "Thank you for being on ${Constant.DOMAIL_URL},<br>" +
                    "<br>" +
                    "Your username is <b>${user.username}</b><br>" +
                    "<br>" +
                    "Please click the link below to reset your password.<br>" +
                    "<br>" +
                    "${Constant.DOMAIL_URL}/user/resetPassword?userId=${user.id}&authKey=${user.authenticationKey}<br>" +
                    "<br>" +
                    "Regards<br>" +
                    "Link Sharing Administration"

        }
    }

    String resetPasswordPage(long userId,String authKey) {
        String message;
        User user = User.findById(userId);
        if(user) {
            if(authKey.equals(user.authenticationKey)) {
                message = "";
            } else {
                message = "Invalid/Old url for password reset, please contact Admin."
            }
        } else {
            message="User does not exists.";
        }
        return message;
    }

    String changePassword(String userId,String authKey,String password1,String password2) {
        String message;
        User user = User.findById(userId);
        if(user) {
            if(authKey.equals(user.authenticationKey)) {
                if(password1 && password1.equals(password2)) {
                    password1 = CommonUtils.generateMd5(password1);
                    user.password = password1;
                    user.save(flush: true);
                } else {
                    message = "Password and Conform Password does not match."
                }
            } else {
                message = "Invalid/Old url for password reset, please contact Admin."
            }
        } else {
            message="User does not exists.";
        }
        return message;
    }

    String authenticateUser(LoginCO loginCO) {
        User user = User.findByUsernameAndPassword(loginCO.username,loginCO.password);
        if(user) {
            if(!user.verified) {
                return "Please verify your email address by clicking on the link sent to you in inbox."
            }else if(!user.active) {
                return "User login inactive."
            } else {
                loginCO.userId = user.id;
                return ""
            }
        } else {
            return "Incorrect Username and/or Password."
        }
    }

    boolean isUsernameAvailable(String username,User user) {
        if(user) {
            User.findByUsernameAndIdNotEqual(username,user.id)?false:true
        } else {
            User.findByUsername(username)?false:true
        }
    }

    boolean isUserEmailAvailable(String email) {
        User.findByEmail(email)?false:true
    }

    boolean isEmailRegistered(String email) {
        User.findByEmail(email)?true:false;
    }

    List<UserDTO> getUserList() {
        List<User> userList = User.findAll();
        List<UserDTO> userDTOList = new ArrayList<>();
        UserDTO userDTO;
        userList.each {user->
            userDTO = new UserDTO(id: user.id,username: user.username,firstName: user.firstName,lastName: user.lastName,
                    email: user.email,profilePicPath: user.profilePicPath,admin: user.admin,active: user.active);
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    String activateDeactivateUser(int userId,String status) {
        int updateCount;
        if("activate".equals(status)) {
            updateCount = User.executeUpdate("update User set active=true where id = :id",[id:userId]);
            if(updateCount) {
                return "activated"
            }
        } else {
            updateCount = User.executeUpdate("update User set active=false where id = :id",[id:userId]);
            if(updateCount) {
                return "deactivated"
            }
        }
    }

    UserDTO getUserDTO(long userId) {
        return createUserDTO(User.findById(userId));
    }

    User getUser(long userId) {
        return User.findById(userId);
    }

    UserDTO createUserDTO(User user) {
        UserDTO userDTO = new UserDTO(id: user.id, username: user.username, firstName: user.firstName, lastName: user.lastName,
                profilePicPath: user.profilePicPath, admin: user.admin, active: user.active);
        userDTO.subscriptionCount = topicService.getTopicCount(user, "subscribed");
        userDTO.topicCount = topicService.getTopicCount(user, "created");
        return userDTO;
    }

    int getTotalUserPublicTopics(long userId) {
        return Topic.countByCreatedByAndVisibility(User.load(userId),Visibility.PUBLIC);
    }

    int getTotalUserPublicResources(long userId) {
        Resource.createCriteria().count(){
            topic{
                eq('visibility',Visibility.PUBLIC)
                eq('createdBy',User.load(userId))
            }
        }
    }

    List<TopicDTO> getUserPublicTopics(long userId, User loggedInUser,int offset, int max) {
        List<Topic> topicList = Topic.findAllByCreatedByAndVisibility(User.load(userId),Visibility.PUBLIC,[offset:offset,max:max]);
        return topicService.createTopicDTO(topicList, topicService.getSubscribersCountMap(), loggedInUser)
    }

    List<ResourceDTO> getUserPublicResources(long userId, User loggedInUser, int offset, int max) {
        List<Resource> resourceList = Resource.createCriteria().list(){
            topic{
                eq('visibility',Visibility.PUBLIC)
            }
            eq('createdBy',User.load(userId))
            firstResult(offset)
            maxResults(max)
            order('id','desc')
        }
        return resourceService.createResourceDTOList(resourceList,loggedInUser)
    }

    User updateProfile(RegisterUserCO userCO,long userId) {
        User user = User.findById(userId);
        if(userCO) {
            user.firstName = userCO.firstName;
            user.lastName = userCO.lastName;
            user.username = userCO.username;
            user.save(flush: true);
            if(userCO.photo.fileItem.name) {
                userCO.uploadPhoto(user.id);
                user.profilePicPath = userCO.fileLocation;
                user.save(flush: true);
            }
            return user;
        }
        return null;
    }

    User updatePassword(String password,long userId) {
        User user = User.findById(userId);
        user.password = password;
        user.save(flush: true);
    }

}
