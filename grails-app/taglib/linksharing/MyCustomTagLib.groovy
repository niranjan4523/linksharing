package linksharing

class MyCustomTagLib {
    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "custom"
    def lowerCase = { attr,body ->
       out << (attr.str as String)?.toUpperCase();
    }
}
