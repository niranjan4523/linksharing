package com.ttnd.linksharing

import com.ttnd.linksharing.enums.Seriousness
import groovy.transform.ToString

/**
 * Created by niranjan on 14/1/16.
 */
@ToString
class Subscription {
    Seriousness seriousness;
    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [topic:Topic,user:User]

    static mapping = {
        seriousness enumType: 'ordinal'
    }
}
