package com.ttnd.linksharing

class Invitation {

    String authenticationKey;
    boolean invitationCompleted;
    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [invitedUser:User,invitedBy:User,topic:Topic]

    static constraints = {
        authenticationKey(nullable: true,blank: true)
    }
}
