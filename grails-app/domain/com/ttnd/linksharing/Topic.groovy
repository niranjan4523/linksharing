package com.ttnd.linksharing

import com.ttnd.linksharing.enums.Visibility
import com.ttnd.linksharing.resource.Resource
import groovy.transform.ToString

/**
 * Created by niranjan on 14/1/16.
 */
@ToString
class Topic {
    String name;
    Visibility visibility;
    Date dateCreated;

    Date lastUpdated;
    static belongsTo = [createdBy:User]
    static hasMany = [subscriber:Subscription,resource:Resource]

    static mapping = {
        visibility enumType: 'ordinal'
    }

    static constraints = {
        name (unique: true)
    }


}
