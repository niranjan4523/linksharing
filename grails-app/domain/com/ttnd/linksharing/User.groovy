package com.ttnd.linksharing

import com.ttnd.linksharing.resource.Resource
import groovy.transform.ToString

@ToString
class User {

    String email;
    String username;
    String password;
    String firstName;
    String lastName;
    boolean admin;
    boolean active;
    String profilePicPath="/images/profile/user-default.png";
    boolean verified;
    String authenticationKey;
    Date dateCreated;
    Date lastUpdated;

    static hasMany = [topic:Topic,subsription:Subscription,resource:Resource,invitedUsers:Invitation,invitationSenders:Invitation]
    static constraints = {
        username (unique: true)
        email (unique: true)
        profilePicPath (nullable: true,blank: true)
        authenticationKey (nullable: true,blank: true)
    }
    static mappedBy = [
            invitedUsers: 'invitedUser',
            invitationSenders: 'invitedBy'
    ]
}
