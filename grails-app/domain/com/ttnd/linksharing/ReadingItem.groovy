package com.ttnd.linksharing

import com.ttnd.linksharing.resource.Resource
import groovy.transform.ToString

/**
 * Created by niranjan on 14/1/16.
 */
@ToString
class ReadingItem {
    User user;
    boolean isRead;

    static belongsTo = [resource:Resource]

    static constraints = {

    }

}
