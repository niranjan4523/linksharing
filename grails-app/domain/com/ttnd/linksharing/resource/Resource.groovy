package com.ttnd.linksharing.resource

import com.ttnd.linksharing.ReadingItem
import com.ttnd.linksharing.Topic
import com.ttnd.linksharing.User

/**
 * Created by niranjan on 14/1/16.
 */

abstract class  Resource {
    String description;
    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [createdBy:User,topic:Topic];
    static hasMany = [ratings: ResourceRating, readingItems: ReadingItem]
    static mapping = {
        description type: 'text'
    }
}
