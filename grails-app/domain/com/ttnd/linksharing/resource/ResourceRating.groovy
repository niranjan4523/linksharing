package com.ttnd.linksharing.resource

import com.ttnd.linksharing.User

/**
 * Created by niranjan on 15/1/16.
 */
class ResourceRating {
    int rating;
    Date dateCreated;
    Date lastUpdated;

    static belongsTo = [resource:Resource,user:User]

}
