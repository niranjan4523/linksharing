package com.ttnd.linksharing.resource

import groovy.transform.ToString

/**
 * Created by niranjan on 14/1/16.
 */
@ToString
class LinkResource extends Resource {
    String url;
}
