package linksharing

import com.ttnd.linksharing.Invitation
import com.ttnd.linksharing.Topic
import com.ttnd.linksharing.User
import com.ttnd.linksharing.co.TopicCO
import com.ttnd.linksharing.dto.TopicDTO

class TopicController {

    TopicService topicService;
    ResourceService resourceService;

    def index() {}

    def createTopic(TopicCO topicCO) {
        if(topicCO.validate()) {
            if(topicService.findTopicByName(topicCO.topicName)) {
                flash.error = "Topic with same name already exists"
            } else {
                topicCO.user = session.user;
                Topic topic = topicService.createTopic(topicCO);
                if(topic) {
                    flash.message = "Topic created and subscribed successfully."
                } else {
                    flash.error = "Some error occurred while creating topic."
                }
            }
        } else {
            flash.error = "Please provide all mandatory fields to create topic."
        }
        redirect(controller: "dashboard",action: "index")
    }

    def isTopicAvailable(String topicName) {
        println("topicName :: ${topicName}")
        if(topicName) {
            render topicService.findTopicByName(topicName)?false:true
        } else {
            render "Please provide topic name."
        }
    }

    def sendInviteMail(String inviteEmail,long inviteTopic) {
        User user = session.user;
        String message = topicService.checkForOtherInvites(inviteEmail,inviteTopic);
        if(message) {
            flash.message = message;
        } else {
            Invitation invitation = topicService.sendTopicInviteMail(inviteEmail,inviteTopic,user)
            if(invitation) {
                flash.message = "Invitation sent to ${inviteEmail}."
            } else {
                flash.error = "Some error occurred during invitation."
            }
        }
        redirect(controller: "dashboard",action: "index")
    }

    def viewOneTopic(long topicId) {
        User user = session.user;
        TopicDTO topicDTO = topicService.getOneTopic(topicId,user)
        int resourceCount = resourceService.getTotalResourceCount(topicId)
        List<TopicDTO> subscribedTopics = topicService.getSubscribedTopicList(user,0,5);
        render (view: 'viewOneTopic', model: [topic: topicDTO,totalResources:resourceCount,subscribedTopics:subscribedTopics])
    }

    def viewAllTopics(String type,long userId) {
        User user = session.user;
        String heading;
        int totalTopicCount = topicService.getTotalTopicCount(user,"subscribed");
        List<TopicDTO> topicDTOList = [];
        if("subscribed".equals(type)) {
            topicDTOList = topicService.getSubscribedTopicList(user,0,10);
            heading = "All Subscriptions";
        } else if ("user".equals(type)){
            topicDTOList = topicService.getCreatedTopicList(User.load(userId),0,10)
            heading = "All Topics by "+topicService.getUserFullName(userId);
        }
        List<TopicDTO> subscribedTopics = topicService.getSubscribedTopicList(user,0,5);

        render(view: "/admin/showTopics",model:[heading:heading,totalTopicCount:totalTopicCount,topicDTOList:topicDTOList,subscribedTopics:subscribedTopics])
    }

    def topicSubscriptionChange(long topicId,String event) {
        render (topicService.topicSubscriptionChange(topicId,session.user,event))
    }

    def topicVisibilityChange(long topicId,int visibility) {
        render (topicService.topicVisibilityChange(topicId,visibility))
    }

    def topicSeriousnessChange(long topicId,int seriousness) {
        render (topicService.topicSeriousnessChange(topicId,session.user,seriousness))
    }

    def topicInviteUser(long topicId,String email) {
        render (topicService.topicInviteUser(topicId,session.user,email))
    }

    def topicNameEdit(long topicId,String topicName){
        render (topicService.topicNameEdit(topicId,topicName,session.user))
    }

    def topicDelete(long topicId) {
        render (topicService.topicDelete(topicId))
    }

}

