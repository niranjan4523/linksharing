package linksharing

import com.ttnd.linksharing.Invitation
import com.ttnd.linksharing.co.LoginCO
import com.ttnd.linksharing.co.RegisterUserCO
import com.ttnd.linksharing.dto.ResourceDTO
import com.ttnd.linksharing.dto.TopicDTO
import com.ttnd.linksharing.dto.UserDTO
import com.ttnd.linksharing.User
import com.ttnd.linksharing.util.CommonUtils
import grails.plugin.mail.MailService

class UserController {

    UserService userService;
    TopicService topicService;
    ResourceService resourceService;
    MailService mailService;

    static  defaultAction = "login"

    def login() {
        List<ResourceDTO> recentShareList = resourceService.getRecentlySharedPosts();
        Date topPostViewDate = new Date();
        topPostViewDate.clearTime();
        List<ResourceDTO> topPosts = resourceService.getTopPosts(topPostViewDate);
        render (view: 'login',model: [recentShareList:recentShareList,topPosts:topPosts])
    }

    def showTopPosts(int topPostView) {
        Date topPostViewDate = new Date();
        topPostViewDate.clearTime();
        if(topPostView == 1) {
            topPostViewDate -= 7;
        } else if(topPostView == 2) {
            topPostViewDate -= 30;
        } else if(topPostView == 3) {
            topPostViewDate -= 365;
        }
        List<ResourceDTO> topPosts = resourceService.getTopPosts(topPostViewDate);
        render (view: 'showTopPosts',model: [topPosts:topPosts]);
    }

    def searchResources(String searchKey) {
        User user = session.user;
        int totalRecord = resourceService.getTotalSearchResultsCount(searchKey,user);
        List<ResourceDTO> listSearchResults = resourceService.searchResources(searchKey,user,0,10);
        List<TopicDTO> trendingTopics = topicService.getTrendingTopicList(user,0,5);
        render (view: 'searchResources',model: [searchKey:searchKey,totalRecord:totalRecord,trendingTopics:trendingTopics,listSearchResults:listSearchResults]);
    }

    def searchResourcesAjax(String searchKey,int offset,int max) {
        User user = session.user;
        int totalRecord = resourceService.getTotalSearchResultsCount(searchKey,user);
        List<ResourceDTO> listSearchResults = resourceService.searchResources(searchKey,user,offset,max);
        render (view: '/resource/_showSearchedPosts',model: [searchKey:searchKey,totalRecords:totalRecord,resourceDTOList:listSearchResults]);
    }
    def acceptInvite(long id,String authKey) {
        Invitation invitation = topicService.acceptInvite(id,authKey);
        if(invitation){
            flash.message = "Subscribed to Topic ${invitation.topic.name}."
        } else {
            flash.error = "Invalid/Old url for subscription, please contact Admin."
        }
        redirect(controller: "user",action: "login")
    }

    def logout() {
        session.invalidate();
        flash.message = "Logged out successfully."
        redirect(controller: "user",action: "login")
    }

    def authenticate(LoginCO loginCO) {
        if(loginCO.validate()) {
            String authenticateUser = userService.authenticateUser(loginCO);
            if(!authenticateUser) {
                session.user = userService.getUser(loginCO.userId);
                redirect(controller: "dashboard",action: "index")
            } else {
                flash.error = authenticateUser;
                redirect(controller: "user",action: "login")
            }
        } else {
            loginCO.errors.allErrors.each {
                println it
            }
            flash.error = "Please provide both username and password"
            redirect(controller: "user",action: "login")
        }
    }
    def isUsernameAvailable(String username) {
        User user = session.user;
        render userService.isUsernameAvailable(username,user)
    }

    def isUserEmailAvailable(String email) {
        render userService.isUserEmailAvailable(email)
    }

    def isEmailRegistered(String email) {
        render userService.isEmailRegistered(email)
    }

    def registerUser(RegisterUserCO registerUserCO) {
        println registerUserCO
        if(registerUserCO.validate()) {
            registerUserCO.fileLocation = grailsApplication.config.file.location
            registerUserCO.authenticationKey = CommonUtils.randomAlphaNumeric(30)
            if(userService.registerUser(registerUserCO)){
                flash.message = "User created successfully,please verify your email by clicking on the link sent in your inbox."
            } else {
                flash.error = "User already registered with same id."
            }
        } else {
            flash.error = "Please fill all mandatory fields."
        }
        redirect(controller: "user",action: "login")
    }

    def verifyAndActivateUser(long userId,String authKey) {
        Map messageMap = userService.verifyAndActivateUser(userId,authKey);
        flash.error = messageMap.error;
        flash.message = messageMap.message;
        redirect(controller: "user",action: "login")
    }

    def forgotPassword() {

    }

    def sendResetPasswordLink(String email) {
        String message = userService.sendResetPasswordLink(email);
        if(message) {
            flash.error = message;
            render(view: 'forgotPassword');
        } else {
            flash.message = "Reset Password link sent."
            redirect(controller: 'user',action: 'login')
        }
    }

    def resetPassword(long userId,String authKey) {
        String message = userService.resetPasswordPage(userId,authKey);
        if(message) {
            flash.error = message;
            redirect(controller: 'user',action: 'login')
        } else {
            render (view: 'resetPassword',model: [userId:userId,authKey:authKey])
        }
    }

    def changeResetPassword(String userId,String authKey,String password1,String password2) {
        String message = userService.changePassword(userId,authKey,password1,password2);
        if("Password and Conform Password does not match.".equals(message)) {
            flash.error = message;
            render (view: 'resetPassword',model: [userId:userId,authKey:authKey])
        } else if(message) {
            flash.error = message;
            redirect(controller: 'user',action: 'login')
        } else {
            flash.message = "Password changed successfully.";
            redirect(controller: 'user',action: 'login')
        }
    }

    def inviteUser() {

    }

    def editProfile() {
        User user = session.user;
        List<TopicDTO> subscribedTopics = topicService.getSubscribedTopicList(user,0,5);
        int totalTopicCount = topicService.getTotalTopicCount(user,"created");
        List<TopicDTO> createdTopicList = topicService.getCreatedTopicList(user,0,10);
        UserDTO userDTO = userService.createUserDTO(user);
        render(view: "editProfile",model:[user:userDTO,subscribedTopics:subscribedTopics,totalTopicCount:totalTopicCount,createdTopicList:createdTopicList])
    }

    def updateProfile(RegisterUserCO userCO) {
        User user = session.user;
        userCO.fileLocation = grailsApplication.config.file.location
        User updatedUser = userService.updateProfile(userCO,user.id);
        if(updatedUser) {
            session.user = updatedUser;
            flash.message = "Profile updated successfully."
        } else {
            flash.error = "Some error occurred."
        }
        redirect(action: 'editProfile')
    }

    def changePassword(String password1,String password2) {
        User user = session.user;
        if(password1.equals(password2)) {
            userService.updatePassword(CommonUtils.generateMd5(password1),user.id);
            flash.message = "Password updated successfully."
        } else {
            flash.error = "Please confirm password correctly."
        }
        redirect(action: 'editProfile')
    }

    def showSubscribedUsers() {

    }

    def showResources() {

    }

    def showUser(long userId) {
        User user = userService.getUser(userId);
        if(session.user && session.user.id == userId){
            response.sendRedirect("/")
        }
        UserDTO userDTO = userService.getUserDTO(userId)
        List<TopicDTO> subscribedTopics = topicService.getSubscribedTopicList(session.user,0,5);
        int totalUserPublicTopics = userService.getTotalUserPublicTopics(userId);
        int totalUserPublicResources = userService.getTotalUserPublicResources(userId);
        List<TopicDTO> userPublicTopics = userService.getUserPublicTopics(userId,user,0,10);
        List<ResourceDTO> userPublicResources = userService.getUserPublicResources(userId,user,0,10);
        render(view: 'showUser',model: [user:userDTO,totalTopicCount: totalUserPublicTopics,totalResourceCount:totalUserPublicResources,
                                        subscribedTopics:subscribedTopics,userPublicTopics:userPublicTopics,userPublicResources:userPublicResources])
    }

    def showUserPosts(long userId,int offset,int max) {
        /*User user = session.user;
        UserDTO userDTO = userService.getUserDTO(userId)
        int totalUserPublicResources = userService.getTotalUserPublicResources(userId);
        List<ResourceDTO> userPublicResources = userService.getUserPublicResources(userId,user,0,10);
        render(view: '_userPosts',model: [])*/
    }

}
