package linksharing

import com.ttnd.linksharing.co.DocResourceCO
import com.ttnd.linksharing.co.LinkResourceCO
import com.ttnd.linksharing.dto.ResourceDTO
import com.ttnd.linksharing.dto.TopicDTO
import com.ttnd.linksharing.User
import com.ttnd.linksharing.resource.Resource

class ResourceController {

    ResourceService resourceService;
    TopicService topicService;

    def index() {}

    def createLinkResource(LinkResourceCO resourceCO) {
        if(resourceCO.validate()) {
            resourceCO.user = session.user;
            if(resourceService.createLinkResource(resourceCO)) {
                flash.message = "Link shared successfully in ${resourceCO.topicName}."
            } else {
                flash.error = "Some error occurred while sharing link."
            }
        } else {
            flash.error = "Please specify all mandatory fields."
        }
        redirect(controller: "dashboard",action: "index")
    }

    def createDocResource(DocResourceCO resourceCO) {
        if(resourceCO.validate()) {
            resourceCO.user = session.user;
            resourceCO.fileLocation = grailsApplication.config.file.location;
            if(resourceService.createDocResource(resourceCO)) {
                flash.message = "Document shared successfully in ${resourceCO.topicName}."
            } else {
                flash.error = "Some error occurred while sharing document."
            }
        } else {
            flash.error = "Please specify all mandatory fields."
        }
        redirect(controller: "dashboard",action: "index")
    }

    def toggleReadStatus(int postId,String event) {
        render (resourceService.resourceReadChange(session.user.id,postId,event))
    }

    def downloadFile(long resourceId) {
        Resource resource = resourceService.getDocResource(resourceId);
        def file = new File(resource.filePath);
        if (file.exists())
        {
            response.setContentType("application/octet-stream") // or or image/JPEG or text/xml or whatever type the file is
            response.setHeader("Content-disposition", "attachment;filename=\"${file.name}\"")
            response.outputStream << file.bytes
        }
        else {
            render "Error!"
        } // appropriate error handling
    }

    def visitUrl(long resourceId) {
        Resource resource = resourceService.getLinkResource(resourceId);
        if(resource.url) {
            redirect(url: resource.url)
        }
    }

    def showPostByTopic(long topicId,int offset,int max) {
        User user = session.user;
        int totalRecords;
        String topicName;
        topicName = resourceService.getTopicName(topicId);
        totalRecords = resourceService.getTotalResourceCount(topicId)
        render (view:'_showPostByTopic',model: [topicId:topicId, totalRecords:totalRecords,
                topicName:topicName, resourceDTOList: resourceService.getTopicResourceList(topicId,user,offset,max)])
    }

    def viewPost(long resourceId) {
        User user = session.user;
        List<TopicDTO> subscribedTopics = topicService.getSubscribedTopicList(user,0,5);
        List<TopicDTO> trendingTopics =  topicService.getTrendingTopicList(user,0,5);
        ResourceDTO resourceDTO = resourceService.getResource(resourceId,user);
        resourceDTO.rating = resourceService.getResourceRating(resourceId,user);
        render (view: 'viewPost',model: [user:user,post: resourceDTO,trendingTopics:trendingTopics,subscribedTopics:subscribedTopics])
    }

    def saveResourceRating(long resourceId,int score) {
        User user = session.user;
        render resourceService.saveResourceRating(resourceId,score,user);
    }

    def resourceDelete(long resourceId) {
        User user = session.user;
        String message = resourceService.deleteResource(resourceId,user);
        if(message) {
            flash.error = message;
            redirect(action: 'viewPost',params: [resourceId:resourceId])
        } else {
            flash.message = "Post deleted successfully."
            redirect(controller: 'dashboard',action: 'index')
        }
    }

    def editPost(long resourceId,String description) {
        User user = session.user;
        String message = resourceService.editResource(resourceId,description,user);
        if(message) {
            flash.error = message;
        }
        redirect(action: 'viewPost',params: [resourceId:resourceId])
    }

}
