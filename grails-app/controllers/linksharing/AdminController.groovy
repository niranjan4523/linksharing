package linksharing

import com.ttnd.linksharing.dto.TopicDTO
import com.ttnd.linksharing.dto.UserDTO
import com.ttnd.linksharing.User

class AdminController {

    UserService userService;
    TopicService topicService;
    static beforeInterceptor = {
        if(session.user && session.user.admin) {
            log.info(":::::::::::::::::::::::Admin user interceptor:::::::::::::::::::::::")
        } else {
            flash.error = "Permission denied."
            redirect(controller: 'dashboard',action: 'index')
        }
    }
    def index() {}
    def showUserList() {
        List<UserDTO> userList = userService.getUserList();
        render (view : "showUsers" , model:[userList: userList])
    }

    def activateDeactivateUser(int userId,String status) {
        render userService.activateDeactivateUser(userId,status);
    }

    def showTopics() {
        User user = session.user;
        int totalTopicCount = topicService.getTotalTopicCount(user,"admin");
        List<TopicDTO> topicDTOList = topicService.getAllTopicList(user,0,10);
        render(view: "showTopics",model:[totalTopicCount:totalTopicCount,topicDTOList:topicDTOList])
    }

    def showPosts() {

    }

}
