package linksharing

import com.ttnd.linksharing.dto.TopicDTO
import com.ttnd.linksharing.dto.UserDTO
import com.ttnd.linksharing.User
import com.ttnd.linksharing.resource.Resource

class DashboardController {

    TopicService topicService;
    ResourceService resourceService;
    def index() {
        User user = session.user;
        List<TopicDTO> subscribedTopics;
        List<TopicDTO> trendingTopics;
        List<Resource> unreadResourceList;
        int totalUnreadPostCount;
        UserDTO userDTO;
        if(user) {
            subscribedTopics = topicService.getSubscribedTopicList(user,0,5);
            trendingTopics =  topicService.getTrendingTopicList(user,0,5);
            unreadResourceList = resourceService.getUnreadResourceList(user,0,10);
            totalUnreadPostCount = resourceService.getTotalUnreadResourceCount(user);
            userDTO = new UserDTO(id: user.id,username: user.username,firstName: user.firstName,lastName: user.lastName,
                    profilePicPath: user.profilePicPath,admin: user.admin,active: user.active);
            userDTO.subscriptionCount = topicService.getTopicCount(user,"subscribed");
            userDTO.topicCount = topicService.getTopicCount(user,"created");
        }
        render(view: "dashboard",model: [user:userDTO, subscribedTopics:subscribedTopics, trendingTopics:trendingTopics, totalUnreadPostCount:totalUnreadPostCount, unreadResourceList:unreadResourceList])
    }

    def showInboxPosts(int offset,int max) {
        User user = session.user;
        render(view: 'template/_inbox',model: [resourceDTOList:resourceService.getUnreadResourceList(user,offset,max),
                                               totalRecords:resourceService.getTotalUnreadResourceCount(user)])
    }
}
