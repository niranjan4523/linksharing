import linksharing.UserService

class BootStrap {
    UserService userService;

    def init = { servletContext ->
        createAdminUser();
    }

    private void createAdminUser() {
        userService.createAdminUser();
    }

    def destroy = {
    }
}
