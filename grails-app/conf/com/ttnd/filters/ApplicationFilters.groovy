package com.ttnd.filters

class ApplicationFilters {

    def filters = {
        all(controller:'*',action:'*',controllerExclude:'user') {
            println "not in user controller"
            before = {
                if(!session.user) {
                    println("Please login to access.")
                    flash.error = "Please login to access."
                    response.sendRedirect("/")
                }
            }
        }

        login(controller: 'user',action: 'login') {
            before = {
                if(session.user) {
                    println("Already logged in ... Redirecting to Dashboard.")
                    response.sendRedirect("/dashboard/index")
                }
            }
        }

    }
}
